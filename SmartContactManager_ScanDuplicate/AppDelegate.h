//
//  AppDelegate.h
//  SmartContactManager_ScanDuplicate
//
//  Created by 8_Sandhya on 11/09/18.
//  Copyright © 2018 Techathalon. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property(strong,nonatomic)IBOutlet UINavigationController *navController;
@property (readonly, strong) NSPersistentContainer *persistentContainer;

- (void)saveContext;


@end

