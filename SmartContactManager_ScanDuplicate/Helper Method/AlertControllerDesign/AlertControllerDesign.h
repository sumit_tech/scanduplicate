//
//  AlertControllerDesign.h
//  SmartContactManager_Backup
//
//  Created by 8_Sandhya on 11/09/18.
//  Copyright © 2018 Techathalon. All rights reserved.
//

#import <Foundation/Foundation.h>
@interface AlertControllerDesign : NSObject
+ (id)sharedObject;
-(void)presentActivityController;
-(void)alertControllerForLoginWithCompletionHandler:(void(^)(NSMutableDictionary *dict, BOOL completed))completionHandler;
-(void)alertControllerForChangePasswordWithCompletionHandler:(void(^)(NSMutableDictionary *dict, BOOL completed))completionHandler;
-(void)alertControllerForLogoutWithCompletionHandler:(void(^)(BOOL completed))completionHandler;
-(void)alertController:(NSString *)title withMessage:(NSString *)message andButtonOne:(NSString *)btn1 andButtontwo:(NSString *)btn2 WithCompletionHandler:(void(^)(BOOL completed))completionHandler;
-(void)alertControllerForSavePasswordWithCompletionHandler:(void(^)(NSMutableDictionary* dict, BOOL completed))completionHandler;
-(void)showAlertControllerWithTitle:(NSString*)title withMessage:(NSString*)message withButtons:(NSArray*)buttArray completionHandler:(void(^)(NSString* string))completionhandler;
@end
