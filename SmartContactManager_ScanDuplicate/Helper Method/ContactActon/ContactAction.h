//
//  ContactAction.h
//  SmartContactManager_Backup
//
//  Created by 8_Sandhya on 03/09/18.
//  Copyright © 2018 Techathalon. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Contacts/Contacts.h> //Contacts.framework for above iOS 9
#import "ContactList.h"
@interface ContactAction : ContactList
-(void)viewOrEditContact:(CNContact *)contact andIsForEdit:(BOOL)isEdit;
+(id)sharedContacts;
-(void)redirectToAddContact:(CNContact *)contact;
-(void)addNewContactToDevice:(NSDictionary *)dict_contact andSaveDirectly:(BOOL)isDirectly;
-(void)deleteSingleContact:(CNContact *)contact WithCompletionHandler:(void(^)(CNContact *contact, BOOL completed))completionHandler;
-(void) deleteAllContacts;
-(void)addSingleVCFContact:(NSString *)vcf_str;
-(void)getUpdatedContactAndGoForApiCallWithCompletionHandler:(void(^)(NSMutableArray *arr_contact, BOOL completed))completionHandler;
-(void) scanContactWithCompletionHandler:(void(^)(NSMutableArray *arr_name, NSMutableArray *arr_number, NSMutableArray *arr_email))completionHandler;
-(void)newScanWithCompletionHandler:(void(^)(NSMutableArray *arr_name, NSMutableArray *arr_number, NSMutableArray *arr_email, NSMutableArray *arr_contact, int result))completionHandler;
-(void)saveContact:(CNMutableContact *)contact;
-(void)mergeDuplicateContactByName:(NSDictionary *)dict WithCompletionHandler:(void(^)(CNContact *contact, BOOL completed))completionHandler;
-(void)updateContact:(CNMutableContact*)contact;
-(void)mergeDuplicateContactByNumber:(NSDictionary *)dict WithCompletionHandler:(void(^)(BOOL completed))completionHandler;
-(void)mergeDuplicateContactByEmail:(NSDictionary *)dict WithCompletionHandler:(void(^)(BOOL completed))completionHandler;
-(void) deleteSingleContactFile:(CNContact *)contact;

//FileManipulation


@end
