//
//  ContactList.m
//  ContactsList
//
//  Created by ndot on 11/01/16.
//  Copyright © 2016 Ktr. All rights reserved.
//
//  My blog address: https://ktrkathir.wordpress.com
//
//  Fetch all contacts using both AddressBook.framework and Contacts.framework
//
//
//  This class file will help you to access contacts app persons details.
//  AddressBook.framework and Contacts.framework will create a different type of arrays
//  Please use totalPhoneNumberArray for Contacts.framework one method and another method for AddressBook.framework to listout the contacts.
//
//


#import "ContactList.h"
#import "AppDelegate.h"
#import "SVProgressHUD.h"
#import <ContactsUI/ContactsUI.h>
#import "MasterVC.h"
#define DARK_GREEN_3 [UIColor colorWithRed:15.0/225.0 green:120.0/225.0 blue:67.0/255.0 alpha:1]
#define MEDUIM_GREEN_2 [UIColor colorWithRed:17.0/225.0 green:129.0/225.0 blue:72.0/255.0 alpha:1]

@interface ContactList ()

@property(strong,nonatomic)AppDelegate* appD;
@end
@implementation ContactList
@synthesize totalPhoneNumberArray;

#pragma mark - Singleton Methods
+ (id)sharedContacts { //Shared instance method
    
    static ContactList *sharedMyContacts = nil; //create contactsList Object
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{ //for first time create shared instance object
        sharedMyContacts = [[self alloc] init];
    });
    
    return sharedMyContacts;
}

- (id)init { //init method
    if (self = [super init]) {
        totalPhoneNumberArray = [NSMutableArray array]; //init a mutableArray
        dispatch_async(dispatch_get_main_queue(), ^{
            self.appD =(AppDelegate*)[[UIApplication sharedApplication] delegate];
        });
    }
    return self;
}

#pragma mark - Fetch All Contacts from Addressbooks or Contacts framework
//Method of fetch contacts from Addressbooks or Contacts framework
- (void)fetchAllContacts:(BOOL)isLoad WithCompletionHandler:(void(^)(int result, NSMutableArray *arr))completionHandler {
    groupsOfContact = [@[] mutableCopy]; //init a mutable array
    contactStore = [[CNContactStore alloc] init]; //init a contactStore object
    
    CNAuthorizationStatus status = [CNContactStore authorizationStatusForEntityType:CNEntityTypeContacts];
    if( status == CNAuthorizationStatusDenied || status == CNAuthorizationStatusRestricted)
    {
        NSLog(@"access denied");
        [self getPermissionToUser];
        completionHandler(1, nil); // permission not granted
    }else{
        if(status == CNAuthorizationStatusNotDetermined){
            NSLog(@"not determined");
        }
        //Create repository objects contacts
        
        [self newFetchContact:isLoad WithCompletionHandler:^(NSMutableArray *arr, BOOL completed) {

            if(completed){
                completionHandler(2, arr);

            }else{
                completionHandler(1, nil);
            }
        }]; //access co
    }
}

#pragma mark - Contacts.framework method
-(void)newFetchContact:(BOOL)showLoader WithCompletionHandler:(void(^)(NSMutableArray *arr, BOOL completed))completionHandler{
    if(contactStore == nil)
        contactStore = [[CNContactStore alloc]init];
    
    NSMutableArray *phoneNumberArray = [@[] mutableCopy]; // init a mutable array
    
//    NSArray *keys = @[CNContactBirthdayKey,CNContactFamilyNameKey, CNContactGivenNameKey, CNContactPhoneNumbersKey, CNContactMiddleNameKey, CNContactImageDataKey, CNContactEmailAddressesKey, CNContactUrlAddressesKey, CNContactPostalAddressesKey, CNContactNoteKey, CNContactNicknameKey, CNContactOrganizationNameKey, CNContactDepartmentNameKey, CNContactJobTitleKey];
//    // Create a request object
//    CNContactFetchRequest *request = [[CNContactFetchRequest alloc] initWithKeysToFetch:keys];
    
    NSString *containerId = contactStore.defaultContainerIdentifier;
    NSPredicate *predicate1 = [CNContact predicateForContactsInContainerWithIdentifier:containerId];
    NSError *error;
//    NSArray *cnContacts = [[ns alloc]init];
    NSArray *cnContacts = [contactStore unifiedContactsMatchingPredicate:predicate1 keysToFetch:@[[CNContactVCardSerialization descriptorForRequiredKeys],CNContactViewController.descriptorForRequiredKeys] error:&error];
    
//    BOOL success = [contactStore enumerateContactsWithFetchRequest:request
//                                                             error:nil
//                                                        usingBlock:^(CNContact* __nonnull contact, BOOL* __nonnull stop)
//                    {
//                        CNMutableContact *newContact = [contact mutableCopy];
//                        [cnContacts addObject:newContact];
//                    }];
    
    if(error != nil){
        completionHandler(nil, NO);
    }else{
        for(CNContact *contact in cnContacts){
            CNMutableContact *mutContact = [contact mutableCopy];
            NSArray <CNLabeledValue<CNPhoneNumber *> *> *phoneNumbers = contact.phoneNumbers;
            CNLabeledValue<CNPhoneNumber *> *firstPhone = [phoneNumbers firstObject];
            CNPhoneNumber *number = firstPhone.value;
            NSString *string = number.stringValue; // 1234567890
            // NSString *label = firstPhone.label;
            //   NSString *string = [NSString stringWithFormat:@"%@", [[contact.phoneNumbers valueForKey:@"value"] valueForKey:@"digits"]];
            NSString *specialCharacterString = @"*#abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
            NSCharacterSet *specialCharacterSet = [NSCharacterSet  characterSetWithCharactersInString:specialCharacterString];
            if (![string.lowercaseString rangeOfCharacterFromSet:specialCharacterSet].length ||  [[[contact.phoneNumbers valueForKey:@"value"] valueForKey:@"digits"] count] == 0) {
                NSArray *array_ph = [[contact.phoneNumbers valueForKey:@"value"] valueForKey:@"digits"];
                if ([array_ph count]  == 0) {
                    array_ph = [[NSArray alloc]init];
                }
                NSMutableArray *array_eml = [[NSMutableArray alloc]init];
                array_eml=[[contact.emailAddresses valueForKey:@"value"] mutableCopy];
                BOOL stricterFilter = NO;
                NSString *stricterFilterString = @"^[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}$";
                NSString *laxString = @"^.+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2}[A-Za-z]*$";
                NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
                NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
                for (int i =0; i<[array_eml count]; i++) {
                    BOOL checkEmail= [emailTest evaluateWithObject:[array_eml objectAtIndex:i]];
                    if(!checkEmail)
                    {
                        if([array_eml isKindOfClass:[NSMutableArray class]])
                            [array_eml removeObjectAtIndex:i];
                    }
                }
                if ([array_eml count]  == 0) {
                    array_eml = [[NSMutableArray alloc]init];
                }
                NSString *fname = [self handleNullValueOfString:contact.givenName];
                
                NSString *givenName = [self handleNullValueOfString:contact.givenName];
                //[contact.givenName stringByTrimmingCharactersInSet: [NSCharacterSet symbolCharacterSet]];
                NSString *lname = [self handleNullValueOfString:contact.familyName]; //[contact.familyName stringByTrimmingCharactersInSet: [NSCharacterSet symbolCharacterSet]];
                NSString *nickname = [self handleNullValueOfString:contact.nickname]; // [contact.nickname stringByTrimmingCharactersInSet: [NSCharacterSet symbolCharacterSet]];
                NSString *organizationName = [self handleNullValueOfString:contact.organizationName]; // [contact.organizationName stringByTrimmingCharactersInSet: [NSCharacterSet symbolCharacterSet]];
                NSString *departmentName = [self handleNullValueOfString:contact.departmentName]; //[contact.departmentName stringByTrimmingCharactersInSet: [NSCharacterSet symbolCharacterSet]];
                NSString *jobTitle = [self handleNullValueOfString:contact.jobTitle]; //[contact.jobTitle stringByTrimmingCharactersInSet: [NSCharacterSet symbolCharacterSet]];
                fname=[self stringFormating:fname];
                lname=[self stringFormating:lname];
                NSString *name= @"";
                if (fname.length > 0 && ![fname isEqualToString:@" "] && lname.length > 0 && ![lname isEqualToString:@" "] ) {
                    name = [fname stringByAppendingString:[NSString stringWithFormat:@" %@",lname]];
                }else if (fname.length > 0 && ![fname isEqualToString:@" "] )
                {
                    name = fname;
                }
                else if (lname.length > 0 && ![lname isEqualToString:@" "] )
                {
                    name = lname;
                }else if (lname.length == 0 && fname.length ==0 && nickname.length >0 && ![nickname isEqualToString:@" "]) {
                    name = [NSString stringWithFormat:@"%@",nickname];
                }else if (name.length ==0)
                {
                    name = @"No Name";
                }
                if(organizationName.length >0 && (fname.length >0 || lname.length >0 || nickname.length>0))
                {
                    name = [NSString stringWithFormat:@"%@ (%@)",name,organizationName];
                }else if (organizationName.length >0 && fname.length == 0 && lname.length == 0 && nickname.length == 0)
                {
                    name = [NSString stringWithFormat:@"%@",organizationName];
                }else if(departmentName.length >0 && name.length==0)
                {
                    name = [NSString stringWithFormat:@"%@ %@", name,departmentName];
                }else if(jobTitle.length >0 && name.length==0)
                {
                    name = [NSString stringWithFormat:@"%@",jobTitle];
                }
                name=[self stringFormating:name];
                mutContact.givenName = name;
                
                NSString * street, *city,* state,* postalCode,* country,* countryCode,*add1;
                NSArray *array_address = contact.postalAddresses;
                
                if([array_address count]>0)
                {
                    CNLabeledValue *value = [array_address objectAtIndex:0] ;
                    CNPostalAddress *postalAdd = value.value;
                    
                    street = [self handleNullValueOfString:postalAdd.street]; //[postalAdd.street stringByTrimmingCharactersInSet: [NSCharacterSet symbolCharacterSet]] ;
                    if (street.length ==0 || street == nil) {
                        street = @"";
                    }
                    
                    city =  [self handleNullValueOfString:postalAdd.city]; //[postalAdd.city stringByTrimmingCharactersInSet: [NSCharacterSet symbolCharacterSet]] ;
                    if (city.length ==0 || city == nil) {
                        city = @"";
                    }
                    
                    state = [self handleNullValueOfString:postalAdd.state]; //[postalAdd.state stringByTrimmingCharactersInSet: [NSCharacterSet symbolCharacterSet]] ;
                    if (state.length ==0 || state == nil) {
                        state = @"";
                    }
                    
                    postalCode = [self handleNullValueOfString:postalAdd.postalCode]; //[postalAdd.postalCode stringByTrimmingCharactersInSet: [NSCharacterSet symbolCharacterSet]] ;
                    if (postalCode.length ==0 || postalCode == nil) {
                        postalCode = @"";
                    }
                    
                    country = [self handleNullValueOfString:postalAdd.country]; //[postalAdd.country stringByTrimmingCharactersInSet: [NSCharacterSet symbolCharacterSet]] ;
                    if (country.length ==0 || country == nil) {
                        country = @"";
                    }
                    
                    countryCode = [self handleNullValueOfString:postalAdd.ISOCountryCode]; //[postalAdd.ISOCountryCode stringByTrimmingCharactersInSet: [NSCharacterSet symbolCharacterSet]] ;
                    if (countryCode.length ==0 || countryCode == nil) {
                        countryCode = @"";
                    }
                    
                    add1 = [NSString stringWithFormat:@"%@ %@",country,countryCode];
                }else
                {
                    street = @"";
                    city = @"";
                    state = @"";
                    postalCode = @"";
                    country = @"";
                    countryCode = @"";
                    add1 = [NSString stringWithFormat:@"%@ %@",country,countryCode];
                }
                NSString  *organiztnName = [self handleNullValueOfString:contact.organizationName]; //[contact.organizationName stringByTrimmingCharactersInSet: [NSCharacterSet symbolCharacterSet]] ;
                if (organiztnName.length ==0 || organiztnName == nil) {
                    organiztnName = @"";
                }
//                NSString *note =[self handleNullValueOfString:contact.note]; //[contact.note stringByTrimmingCharactersInSet: [NSCharacterSet symbolCharacterSet]] ;;
//                note=[self stringFormating:note];
//                if (note.length ==0 || note == nil) {
//                    note = @"";
//                }else{
//                    NSLog(@"%@",note);
//                }
//                NSString *note =@"";
                if(showLoader){
                    dispatch_async(dispatch_get_main_queue(), ^{
                        NSInteger per = (phoneNumberArray.count * 100)/cnContacts.count;
                        [SVProgressHUD showProgress:(float)per/100.0 status:[NSString stringWithFormat:@"%lu%@", per,@"%"]];
                    });
                }
                
                //                            if(([array_eml count]>0 || [array_ph count]>0))  //name.length >0 &&
                //                            {
                NSDictionary *peopleDic = @{@"name":name,
                                            //                          @"u_image":[contact.thumbnailImageData base64EncodedStringWithOptions:0] != nil ? [contact.thumbnailImageData base64EncodedStringWithOptions:0]:@"",
                                            @"phoneNumbers":array_ph,
                                            @"emailAddresses":array_eml,
                                            @"city":city,
                                            @"street":street,
                                            @"address2":add1,
                                            @"state":state,
                                            @"postalCode":postalCode,
                                            @"company":organiztnName,
//                                            @"note":note,
                                            @"givenName":givenName,
                                            @"middleName":fname,
                                            @"familyName":lname,
                                            @"job_title":jobTitle,
                                            @"department_name":departmentName,
                                            @"nickname":nickname,
                                            @"country":country,
                                            @"contact":[mutContact copy],
                                            //                          @"selected":@"NO"
                                            };
                [phoneNumberArray addObject:peopleDic]; //add object of people info to array
                //                            }
            }
        }
        self->totalPhoneNumberArray = phoneNumberArray ;
        completionHandler(self->totalPhoneNumberArray, YES);
    }
}


/*
-(void) fetchContactsFromContactsFrameWork:(BOOL)showLoader WithCompletionHandler:(void(^)(NSMutableArray *arr, BOOL completed))completionHandler{ //access contacts using contacts.framework
    
    if(contactStore == nil)
        contactStore = [[CNContactStore alloc] init];
    
    NSMutableArray *phoneNumberArray = [@[] mutableCopy]; // init a mutable array

    @try {
        NSMutableArray *__block allContact = [[NSMutableArray alloc]init];
        
        [contactStore requestAccessForEntityType:CNEntityTypeContacts completionHandler:^(BOOL granted, NSError * _Nullable error) {
            if(granted){
                NSString *containerId = self->contactStore.defaultContainerIdentifier;
                NSPredicate *predicate = [CNContact predicateForContactsInContainerWithIdentifier:containerId];
                NSError *error;
                allContact = [[self->contactStore unifiedContactsMatchingPredicate:predicate keysToFetch:@[[CNContactVCardSerialization descriptorForRequiredKeys],CNContactViewController.descriptorForRequiredKeys] error:&error] mutableCopy];
                if(error)
                    NSLog(@"error fetching contacts %@", error);
                else{
                    for(CNContact *contact in allContact){
                        CNMutableContact *mutContact = [contact mutableCopy];
                        NSArray <CNLabeledValue<CNPhoneNumber *> *> *phoneNumbers = contact.phoneNumbers;
                        CNLabeledValue<CNPhoneNumber *> *firstPhone = [phoneNumbers firstObject];
                        CNPhoneNumber *number = firstPhone.value;
                        NSString *string = number.stringValue; // 1234567890
                        // NSString *label = firstPhone.label;
                        //   NSString *string = [NSString stringWithFormat:@"%@", [[contact.phoneNumbers valueForKey:@"value"] valueForKey:@"digits"]];
                        NSString *specialCharacterString = @"*#abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
                        NSCharacterSet *specialCharacterSet = [NSCharacterSet  characterSetWithCharactersInString:specialCharacterString];
                        if (![string.lowercaseString rangeOfCharacterFromSet:specialCharacterSet].length ||  [[[contact.phoneNumbers valueForKey:@"value"] valueForKey:@"digits"] count] == 0) {
                            NSArray *array_ph = [[contact.phoneNumbers valueForKey:@"value"] valueForKey:@"digits"];
                            if ([array_ph count]  == 0) {
                                array_ph = [[NSArray alloc]init];
                            }
                            NSMutableArray *array_eml = [[NSMutableArray alloc]init];
                            array_eml=[[contact.emailAddresses valueForKey:@"value"] mutableCopy];
                            BOOL stricterFilter = NO;
                            NSString *stricterFilterString = @"^[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}$";
                            NSString *laxString = @"^.+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2}[A-Za-z]*$";
                            NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
                            NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
                            for (int i =0; i<[array_eml count]; i++) {
                                BOOL checkEmail= [emailTest evaluateWithObject:[array_eml objectAtIndex:i]];
                                if(!checkEmail)
                                {
                                    if([array_eml isKindOfClass:[NSMutableArray class]])
                                        [array_eml removeObjectAtIndex:i];
                                }
                            }
                            if ([array_eml count]  == 0) {
                                array_eml = [[NSMutableArray alloc]init];
                            }
                            NSString *fname = [self handleNullValueOfString:contact.givenName];
                            
                            NSString *givenName = [self handleNullValueOfString:contact.givenName];
                            //[contact.givenName stringByTrimmingCharactersInSet: [NSCharacterSet symbolCharacterSet]];
                            NSString *lname = [self handleNullValueOfString:contact.familyName]; //[contact.familyName stringByTrimmingCharactersInSet: [NSCharacterSet symbolCharacterSet]];
                            NSString *nickname = [self handleNullValueOfString:contact.nickname]; // [contact.nickname stringByTrimmingCharactersInSet: [NSCharacterSet symbolCharacterSet]];
                            NSString *organizationName = [self handleNullValueOfString:contact.organizationName]; // [contact.organizationName stringByTrimmingCharactersInSet: [NSCharacterSet symbolCharacterSet]];
                            NSString *departmentName = [self handleNullValueOfString:contact.departmentName]; //[contact.departmentName stringByTrimmingCharactersInSet: [NSCharacterSet symbolCharacterSet]];
                            NSString *jobTitle = [self handleNullValueOfString:contact.jobTitle]; //[contact.jobTitle stringByTrimmingCharactersInSet: [NSCharacterSet symbolCharacterSet]];
                            fname=[self stringFormating:fname];
                            lname=[self stringFormating:lname];
                            NSString *name= @"";
                            if (fname.length > 0 && ![fname isEqualToString:@" "] && lname.length > 0 && ![lname isEqualToString:@" "] ) {
                                name = [fname stringByAppendingString:[NSString stringWithFormat:@" %@",lname]];
                            }else if (fname.length > 0 && ![fname isEqualToString:@" "] )
                            {
                                name = fname;
                            }
                            else if (lname.length > 0 && ![lname isEqualToString:@" "] )
                            {
                                name = lname;
                            }else if (lname.length == 0 && fname.length ==0 && nickname.length >0 && ![nickname isEqualToString:@" "]) {
                                name = [NSString stringWithFormat:@"%@",nickname];
                            }else if (name.length ==0)
                            {
                                name = @"No Name";
                            }
                            if(organizationName.length >0 && (fname.length >0 || lname.length >0 || nickname.length>0))
                            {
                                name = [NSString stringWithFormat:@"%@ (%@)",name,organizationName];
                            }else if (organizationName.length >0 && fname.length == 0 && lname.length == 0 && nickname.length == 0)
                            {
                                name = [NSString stringWithFormat:@"%@",organizationName];
                            }else if(departmentName.length >0 && name.length==0)
                            {
                                name = [NSString stringWithFormat:@"%@ %@", name,departmentName];
                            }else if(jobTitle.length >0 && name.length==0)
                            {
                                name = [NSString stringWithFormat:@"%@",jobTitle];
                            }
                            name=[self stringFormating:name];
                            mutContact.givenName = name;

                            NSString * street, *city,* state,* postalCode,* country,* countryCode,*add1;
                            NSArray *array_address = contact.postalAddresses;
                            
                            if([array_address count]>0)
                            {
                                CNLabeledValue *value = [array_address objectAtIndex:0] ;
                                CNPostalAddress *postalAdd = value.value;
                                
                                street = [self handleNullValueOfString:postalAdd.street]; //[postalAdd.street stringByTrimmingCharactersInSet: [NSCharacterSet symbolCharacterSet]] ;
                                if (street.length ==0 || street == nil) {
                                    street = @"";
                                }
                                
                                city =  [self handleNullValueOfString:postalAdd.city]; //[postalAdd.city stringByTrimmingCharactersInSet: [NSCharacterSet symbolCharacterSet]] ;
                                if (city.length ==0 || city == nil) {
                                    city = @"";
                                }
                                
                                state = [self handleNullValueOfString:postalAdd.state]; //[postalAdd.state stringByTrimmingCharactersInSet: [NSCharacterSet symbolCharacterSet]] ;
                                if (state.length ==0 || state == nil) {
                                    state = @"";
                                }
                                
                                postalCode = [self handleNullValueOfString:postalAdd.postalCode]; //[postalAdd.postalCode stringByTrimmingCharactersInSet: [NSCharacterSet symbolCharacterSet]] ;
                                if (postalCode.length ==0 || postalCode == nil) {
                                    postalCode = @"";
                                }
                                
                                country = [self handleNullValueOfString:postalAdd.country]; //[postalAdd.country stringByTrimmingCharactersInSet: [NSCharacterSet symbolCharacterSet]] ;
                                if (country.length ==0 || country == nil) {
                                    country = @"";
                                }
                                
                                countryCode = [self handleNullValueOfString:postalAdd.ISOCountryCode]; //[postalAdd.ISOCountryCode stringByTrimmingCharactersInSet: [NSCharacterSet symbolCharacterSet]] ;
                                if (countryCode.length ==0 || countryCode == nil) {
                                    countryCode = @"";
                                }
                                
                                add1 = [NSString stringWithFormat:@"%@ %@",country,countryCode];
                            }else
                            {
                                street = @"";
                                city = @"";
                                state = @"";
                                postalCode = @"";
                                country = @"";
                                countryCode = @"";
                                add1 = [NSString stringWithFormat:@"%@ %@",country,countryCode];
                            }
                            NSString  *organiztnName = [self handleNullValueOfString:contact.organizationName]; //[contact.organizationName stringByTrimmingCharactersInSet: [NSCharacterSet symbolCharacterSet]] ;
                            if (organiztnName.length ==0 || organiztnName == nil) {
                                organiztnName = @"";
                            }
                            NSString *note =[self handleNullValueOfString:contact.note]; //[contact.note stringByTrimmingCharactersInSet: [NSCharacterSet symbolCharacterSet]] ;;
                            note=[self stringFormating:note];
                            if (note.length ==0 || note == nil) {
                                note = @"";
                            }
                            if(showLoader){
                                dispatch_async(dispatch_get_main_queue(), ^{
                                    NSInteger per = (phoneNumberArray.count * 100)/allContact.count;
                                    [SVProgressHUD showProgress:(float)per/100.0 status:[NSString stringWithFormat:@"%lu%@", per,@"%"]];
                                });
                            }
                            
//                            if(([array_eml count]>0 || [array_ph count]>0))  //name.length >0 &&
//                            {
                                NSDictionary *peopleDic = @{@"name":name,
                                                            //                          @"u_image":[contact.thumbnailImageData base64EncodedStringWithOptions:0] != nil ? [contact.thumbnailImageData base64EncodedStringWithOptions:0]:@"",
                                                            @"phoneNumbers":array_ph,
                                                            @"emailAddresses":array_eml,
                                                            @"city":city,
                                                            @"street":street,
                                                            @"address2":add1,
                                                            @"state":state,
                                                            @"postalCode":postalCode,
                                                            @"company":organiztnName,
                                                            @"note":note,
                                                            @"givenName":givenName,
                                                            @"middleName":fname,
                                                            @"familyName":lname,
                                                            @"job_title":jobTitle,
                                                            @"department_name":departmentName,
                                                            @"nickname":nickname,
                                                            @"country":country,
                                                            @"contact":[mutContact copy],
                                                            //                          @"selected":@"NO"
                                                            };
                                [phoneNumberArray addObject:peopleDic]; //add object of people info to array
//                            }
                        }
                    }
                    self->totalPhoneNumberArray = phoneNumberArray ;
                    completionHandler(self->totalPhoneNumberArray, YES);
                }
            }
        }];
    }
    @catch (NSException * e) {
        NSLog(@"Exception on contact list: %@", e);
    }
}
 */
//- (void)fetchContactsFromContactsFrameWork { //access contacts using contacts.framework
//
//    if(contactStore == nil)
//        contactStore = [[CNContactStore alloc] init];
//
//    NSMutableArray *phoneNumberArray = [@[] mutableCopy]; // init a mutable array
//    NSArray *keyToFetch = @[CNContactEmailAddressesKey,CNContactFamilyNameKey,CNContactMiddleNameKey,CNContactGivenNameKey,CNContactPhoneNumbersKey,CNContactPostalAddressesKey,CNContactThumbnailImageDataKey,CNContactOrganizationNameKey,CNContactDepartmentNameKey,CNContactJobTitleKey,CNContactNoteKey,CNContactNameSuffixKey,CNContactNicknameKey]; //contacts list key params to access using contacts.framework
//    CNContactFetchRequest *fetchRequest = [[CNContactFetchRequest alloc] initWithKeysToFetch:keyToFetch];
//    NSError *error;//Contacts fetch request parrams object allocation
//
//    @try {
//        if([contactStore isKindOfClass:[CNContactStore class]]){
//            [contactStore enumerateContactsWithFetchRequest:fetchRequest error:&error usingBlock:^(CNContact * _Nonnull contact, BOOL * _Nonnull stop) {
//
//                if(error)
//                    NSLog(@"error fetching contacts %@", error);
//                else{
//                    NSArray <CNLabeledValue<CNPhoneNumber *> *> *phoneNumbers = contact.phoneNumbers;
//                    CNLabeledValue<CNPhoneNumber *> *firstPhone = [phoneNumbers firstObject];
//                    CNPhoneNumber *number = firstPhone.value;
//                    NSString *string = number.stringValue; // 1234567890
//                    // NSString *label = firstPhone.label;
//                    //   NSString *string = [NSString stringWithFormat:@"%@", [[contact.phoneNumbers valueForKey:@"value"] valueForKey:@"digits"]];
//                    NSString *specialCharacterString = @"*#abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
//                    NSCharacterSet *specialCharacterSet = [NSCharacterSet  characterSetWithCharactersInString:specialCharacterString];
//                    if (![string.lowercaseString rangeOfCharacterFromSet:specialCharacterSet].length ||  [[[contact.phoneNumbers valueForKey:@"value"] valueForKey:@"digits"] count] == 0) {
//                        NSArray *array_ph = [[contact.phoneNumbers valueForKey:@"value"] valueForKey:@"digits"];
//                        if ([array_ph count]  == 0) {
//                            array_ph = [[NSArray alloc]init];
//                        }
//                        NSMutableArray *array_eml = [[NSMutableArray alloc]init];
//                        array_eml=[[contact.emailAddresses valueForKey:@"value"] mutableCopy];
//                        BOOL stricterFilter = NO;
//                        NSString *stricterFilterString = @"^[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}$";
//                        NSString *laxString = @"^.+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2}[A-Za-z]*$";
//                        NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
//                        NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
//                        for (int i =0; i<[array_eml count]; i++) {
//                            BOOL checkEmail= [emailTest evaluateWithObject:[array_eml objectAtIndex:i]];
//                            if(!checkEmail)
//                            {
//                                if([array_eml isKindOfClass:[NSMutableArray class]])
//                                    [array_eml removeObjectAtIndex:i];
//                            }
//                        }
//                        if ([array_eml count]  == 0) {
//                            array_eml = [[NSMutableArray alloc]init];
//                        }
//                        NSString *fname = [self handleNullValueOfString:contact.givenName];//[contact.givenName stringByTrimmingCharactersInSet: [NSCharacterSet symbolCharacterSet]];
//                        NSString *lname = [self handleNullValueOfString:contact.familyName]; //[contact.familyName stringByTrimmingCharactersInSet: [NSCharacterSet symbolCharacterSet]];
//                        NSString *nickname = [self handleNullValueOfString:contact.nickname]; // [contact.nickname stringByTrimmingCharactersInSet: [NSCharacterSet symbolCharacterSet]];
//                        NSString *organizationName = [self handleNullValueOfString:contact.organizationName]; // [contact.organizationName stringByTrimmingCharactersInSet: [NSCharacterSet symbolCharacterSet]];
//                        NSString *departmentName = [self handleNullValueOfString:contact.departmentName]; //[contact.departmentName stringByTrimmingCharactersInSet: [NSCharacterSet symbolCharacterSet]];
//                        NSString *jobTitle = [self handleNullValueOfString:contact.jobTitle]; //[contact.jobTitle stringByTrimmingCharactersInSet: [NSCharacterSet symbolCharacterSet]];
//                        fname=[self stringFormating:fname];
//                        lname=[self stringFormating:lname];
//                        NSString *name= @"";
//                        if (fname.length > 0 && ![fname isEqualToString:@" "] && lname.length > 0 && ![lname isEqualToString:@" "] ) {
//                            name = [fname stringByAppendingString:[NSString stringWithFormat:@" %@",lname]];
//                        }else if (fname.length > 0 && ![fname isEqualToString:@" "] )
//                        {
//                            name = fname;
//                        }
//                        else if (lname.length > 0 && ![lname isEqualToString:@" "] )
//                        {
//                            name = lname;
//                        }else if (lname.length == 0 && fname.length ==0 && nickname.length >0 && ![nickname isEqualToString:@" "]) {
//                            name = [NSString stringWithFormat:@"%@",nickname];
//                        }else if (name.length ==0)
//                        {
//                            name = @"";
//                        }
//                        if(organizationName.length >0 && (fname.length >0 || lname.length >0 || nickname.length>0))
//                        {
//                            name = [NSString stringWithFormat:@"%@ (%@)",name,organizationName];
//                        }else if (organizationName.length >0 && fname.length == 0 && lname.length == 0 && nickname.length == 0)
//                        {
//                            name = [NSString stringWithFormat:@"%@",organizationName];
//                        }else if(departmentName.length >0 && name.length==0)
//                        {
//                            name = [NSString stringWithFormat:@"%@ %@", name,departmentName];
//                        }else if(jobTitle.length >0 && name.length==0)
//                        {
//                            name = [NSString stringWithFormat:@"%@",jobTitle];
//                        }
//
//                        name=[self stringFormating:name];
//                        NSString * street, *city,* state,* postalCode,* country,* countryCode,*add1;
//                        NSArray *array_address = contact.postalAddresses;
//
//                        if([array_address count]>0)
//                        {
//                            CNLabeledValue *value = [array_address objectAtIndex:0] ;
//                            CNPostalAddress *postalAdd = value.value;
//
//                            street = [self handleNullValueOfString:postalAdd.street]; //[postalAdd.street stringByTrimmingCharactersInSet: [NSCharacterSet symbolCharacterSet]] ;
//                            if (street.length ==0 || street == nil) {
//                                street = @"";
//                            }
//
//                            city =  [self handleNullValueOfString:postalAdd.city]; //[postalAdd.city stringByTrimmingCharactersInSet: [NSCharacterSet symbolCharacterSet]] ;
//                            if (city.length ==0 || city == nil) {
//                                city = @"";
//                            }
//
//                            state = [self handleNullValueOfString:postalAdd.state]; //[postalAdd.state stringByTrimmingCharactersInSet: [NSCharacterSet symbolCharacterSet]] ;
//                            if (state.length ==0 || state == nil) {
//                                state = @"";
//                            }
//
//                            postalCode = [self handleNullValueOfString:postalAdd.postalCode]; //[postalAdd.postalCode stringByTrimmingCharactersInSet: [NSCharacterSet symbolCharacterSet]] ;
//                            if (postalCode.length ==0 || postalCode == nil) {
//                                postalCode = @"";
//                            }
//
//                            country = [self handleNullValueOfString:postalAdd.country]; //[postalAdd.country stringByTrimmingCharactersInSet: [NSCharacterSet symbolCharacterSet]] ;
//                            if (country.length ==0 || country == nil) {
//                                country = @"";
//                            }
//
//                            countryCode = [self handleNullValueOfString:postalAdd.ISOCountryCode]; //[postalAdd.ISOCountryCode stringByTrimmingCharactersInSet: [NSCharacterSet symbolCharacterSet]] ;
//                            if (countryCode.length ==0 || countryCode == nil) {
//                                countryCode = @"";
//                            }
//
//                            add1 = [NSString stringWithFormat:@"%@ %@",country,countryCode];
//                        }else
//                        {
//                            street = @"";
//                            city = @"";
//                            state = @"";
//                            postalCode = @"";
//                            country = @"";
//                            countryCode = @"";
//                            add1 = [NSString stringWithFormat:@"%@ %@",country,countryCode];
//                        }
//                        NSString  *organiztnName = [self handleNullValueOfString:contact.organizationName]; //[contact.organizationName stringByTrimmingCharactersInSet: [NSCharacterSet symbolCharacterSet]] ;
//                        if (organiztnName.length ==0 || organiztnName == nil) {
//                            organiztnName = @"";
//                        }
//                        NSString *note =[self handleNullValueOfString:contact.note]; //[contact.note stringByTrimmingCharactersInSet: [NSCharacterSet symbolCharacterSet]] ;;
//                        note=[self stringFormating:note];
//                        if (note.length ==0 || note == nil) {
//                            note = @"";
//                        }
//
//                        if(([array_eml count]>0 || [array_ph count]>0))  //name.length >0 &&
//                        {
//                            NSDictionary *peopleDic = @{@"name":name,
//                                                        //                          @"u_image":[contact.thumbnailImageData base64EncodedStringWithOptions:0] != nil ? [contact.thumbnailImageData base64EncodedStringWithOptions:0]:@"",
//                                                        @"u_phone":array_ph,
//                                                        @"u_email":array_eml,
//                                                        @"city":city,
//                                                        @"address1":street,
//                                                        @"address2":add1,
//                                                        @"state_name":state,
//                                                        @"u_zipcode":postalCode,
//                                                        @"company":organiztnName,
//                                                        @"note":note,
//                                                        @"first_name":fname,
//                                                        @"last_name":lname,
//                                                        @"job_title":jobTitle,
//                                                        @"department_name":departmentName,
//                                                        @"nickname":nickname,
//                                                        //                          @"selected":@"NO"
//                                                        };
//                            [phoneNumberArray addObject:peopleDic]; //add object of people info to array
//                        }
//                    }
//                }
//            }];
//        }
//    }
//    @catch (NSException * e) {
//        NSLog(@"Exception: %@", e);
//    }
//
//    totalPhoneNumberArray = phoneNumberArray ; //get a copy of all contacts list to array.
//}

-(NSString *)handleNullValueOfString:(NSString *)value
{
    value = [value stringByTrimmingCharactersInSet: [NSCharacterSet symbolCharacterSet]];
    value = (value == (id)[NSNull null] || value.length == 0 || [value isEqualToString:@"(null)"]) ?@"":value;
    return value;
}

-(void)getPermissionToUser {
    [SVProgressHUD dismiss];
    UIViewController *contrllr =_appD.navController.visibleViewController;
    // Send an alert telling user to change privacy setting in settings app
    UIAlertController *popup=[UIAlertController alertControllerWithTitle:@"You previously denied access: You must enable access to contacts in settings" message:@"" preferredStyle:UIAlertControllerStyleAlert];
    NSDictionary *attrDict = @{
                               NSFontAttributeName :[UIFont systemFontOfSize:20.0],
                               NSForegroundColorAttributeName : DARK_GREEN_3
                               };
    NSMutableAttributedString *title = [[NSMutableAttributedString alloc] initWithString:@"You previously denied access: You must enable access to contacts in settings" attributes:attrDict];
//    [popup.view setTintColor:MEDUIM_GREEN_2];
    [popup setValue:title forKey:@"attributedTitle"];
    
    UIAlertAction *btn1=[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDestructive handler:^(UIAlertAction * _Nonnull action) {
        [popup dismissViewControllerAnimated:YES completion:nil];
    }];
    UIAlertAction *btn4=[UIAlertAction actionWithTitle:@"Enable" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        if([[UIDevice currentDevice].systemVersion floatValue] >= 10.0){
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString] options:[[NSDictionary alloc]init] completionHandler:^(BOOL success) {
            }]  ;
        } else {
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString] options:@{} completionHandler:nil];
        }
    }];
    [popup addAction:btn1];
    [popup addAction:btn4];
    [contrllr presentViewController:popup animated:YES completion:nil];
}

-(NSString *)stringFormating :(NSString *)string
{
    if(string.length>0)
    {
        //        NSCharacterSet *alphaSet = [NSCharacterSet alphanumericCharacterSet];
        //        [string stringByTrimmingCharactersInSet:alphaSet];
        
        NSCharacterSet *notAllowedChars = [NSCharacterSet characterSetWithCharactersInString:@"/\\,?!@#$%^&*()_+:|<>"];
        NSString *resultString = [[string componentsSeparatedByCharactersInSet:notAllowedChars] componentsJoinedByString:@""];
        
        //        NSLog (@"Result: %@", resultString);
        return resultString;
    }
    else
        return @"";
}

#pragma mark - Delete contacts
-(void) deleteAllContacts {
    NSArray *keys = @[CNContactPhoneNumbersKey,CNContactGivenNameKey];
    NSString *containerId = contactStore.defaultContainerIdentifier;
    NSPredicate *predicate = [CNContact predicateForContactsInContainerWithIdentifier:containerId];
    NSError *error;
    NSArray *cnContactsa = [contactStore unifiedContactsMatchingPredicate:predicate keysToFetch:keys error:&error];
    
    if (error) {
        NSLog(@"error fetching contacts %@", error);
    } else {
        CNSaveRequest *saveRequest = [[CNSaveRequest alloc] init];
        
        for (CNContact *contact in cnContactsa) {
            if([contact.givenName isEqualToString:@"Akdam"])
            {
                [saveRequest deleteContact:[contact mutableCopy]];
                
            }
        }
        [contactStore executeSaveRequest:saveRequest error:nil];
    }
}

#pragma mark - save file
-(void)generateAndSaveVCF:(NSMutableArray *)arr_contact WithUSerID:(NSString *)user_id andCompletionHandler:(void(^)(NSString *filename, BOOL completed))completionHandler
{
    NSMutableArray *arr = [self getVCFString:arr_contact];
    NSError *error;
    NSData *vcarddata =[CNContactVCardSerialization dataWithContacts:arr error:&error];
    NSString* vcardStr = [[NSString alloc] initWithData:vcarddata encoding:NSUTF8StringEncoding];
    [self writeToTextFile:vcardStr WithUserID:user_id andCompletionHandler:^(NSString *filename, BOOL completed) {
        completionHandler(filename, completed);
    }];
}

-(NSMutableArray *)getVCFString:(NSMutableArray *)arr
{
    NSMutableArray *arr_new = [[NSMutableArray alloc]init];
    for(NSDictionary *dict in arr)
    {
        CNContact *contact = (CNContact *)[dict objectForKey:@"contact"];
        [arr_new addObject:contact];
    }
    return arr_new;
}

-(void) writeToTextFile:(NSString *) content WithUserID:(NSString*)user_id andCompletionHandler:(void(^)(NSString *filename, BOOL completed))completionHandler{
//    NSDictionary *dict = [[NSDictionary alloc]init];
    //    dict = [self getdataFromPlist];
//    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
//    [dateFormatter setDateFormat:@"yyyyMMdd_HHmm"];
//    NSDate *todaydate = [NSDate date];
//    NSString *dateString = [dateFormatter stringFromDate:todaydate];
    self.fileName = @"name";//[NSString stringWithFormat:@"SCM_B_%@_%@.vcf",[[PlistHandler sharedObject]getValueFromPlistByKey:@"user_id"],dateString];
    NSString *path = [[[MasterVC sharedInstance] applicationDocumentsDirectory].path stringByAppendingPathComponent:self.fileName];
    [content writeToFile:path atomically:YES encoding:NSUTF8StringEncoding error:nil];
    completionHandler(self.fileName,YES);
    
}
-(void) uploadFile:(NSString *)email_id WithUserID:(NSString*)user_id
{
    NSString *filePath = [[[MasterVC sharedInstance] applicationDocumentsDirectory].path
                          stringByAppendingPathComponent:self.fileName];
    BOOL fileExists = [[NSFileManager defaultManager] fileExistsAtPath:filePath];
    if(fileExists) {
        //NSLog(@"fileExists : %d",fileExists);
        NSString *urlString = uploadTextURL;
        NSData *textData = [NSData dataWithContentsOfFile:filePath];
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init] ;
        [request setURL:[NSURL URLWithString:urlString]];
        [request setHTTPMethod:@"POST"];
        
        NSString *boundary = @"---------------------------14737809831466499882746641449";
        NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@",boundary];
        [request addValue:contentType forHTTPHeaderField: @"Content-Type"];
        
        NSMutableData *body = [NSMutableData data];
        //        NSDictionary *dict =[self getdataFromPlist];
        [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"user_id\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"%@", user_id] dataUsingEncoding:NSUTF8StringEncoding]];
        //        [[PlistHandler sharedObject]getValueFromPlistByKey:@"user_id"]
        [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"email\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[email_id dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        NSString *filename1=[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"userfile\"; filename=\"%@\"\r\n",self.fileName];
        [body appendData:[filename1 dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[@"Content-Type: application/octet-stream\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[NSData dataWithData:textData]];
        [body appendData:[[NSString stringWithFormat:@"\r\n--%@--\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [request setHTTPBody:body];
        /*NSData *returnData = [NSURLConnection sendSynchronousRequest:request returningResponse:nil error:nil];    [[NSOperationQueue mainQueue] addOperationWithBlock: ^ {
         NSMutableDictionary *dict=(NSMutableDictionary *)[NSJSONSerialization JSONObjectWithData:returnData options:0 error:nil];
         BOOL success=[[dict objectForKey:@"success"]boolValue];
         //NSLog(@"dict %@",dict);
         if (success==1) {
         [SVProgressHUD dismiss];
         
         ViewBackupViewController *viewController = [[ViewBackupViewController alloc]initWithNibName:@"ViewBackupViewController" bundle:nil];
         [self.navigationController pushViewController:viewController animated:YES];
         }
         }];*/
        NSURLSession *session = [NSURLSession sessionWithConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
        
        [[session dataTaskWithRequest:request completionHandler:^(NSData *datas, NSURLResponse *response, NSError *error) {
            NSError *err;
            if(datas == nil)
            {
                return;
            }
            NSDictionary *jsonDict = [NSJSONSerialization JSONObjectWithData:datas options:NSJSONReadingAllowFragments error:&err];
            
            BOOL success=[[jsonDict objectForKey:@"success"]boolValue];
            if (!success){
                [SVProgressHUD dismiss];
                return ;
            }
            
            NSLog(@"requestReply: %@", jsonDict);
            dispatch_async(dispatch_get_main_queue(), ^{
                [SVProgressHUD dismiss];
                [SVProgressHUD showSuccessWithStatus:@"VCF file uploaded."];
            });
        }] resume];
    }
}

-(void) uploadFile:(NSString *)email_id WithCompletionHandler:(void (^)(int result, NSMutableDictionary * response))completion
{
    NSString *userID = [NSString stringWithFormat:@"%@", [[PlistHandler sharedObject]getValueFromPlistByKey:@"user_id"]];
    userID = nil;
    NSString *filePath = [[[MasterVC sharedInstance] applicationDocumentsDirectory].path                          stringByAppendingPathComponent:self.fileName];
    
    BOOL fileExists = [[NSFileManager defaultManager] fileExistsAtPath:filePath];
    if(fileExists) {
        //NSLog(@"fileExists : %d",fileExists);
        NSString *urlString = uploadTextURL;
        NSData *textData = [NSData dataWithContentsOfFile:filePath];
        
        if(email_id == nil || email_id.length == 0 || filePath == nil || filePath.length == 0 || userID.length == 0 || userID == nil || textData == nil)
        {
            completion(1, nil);
        }
        
        
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init] ;
        [request setURL:[NSURL URLWithString:urlString]];
        [request setHTTPMethod:@"POST"];
        
        NSString *boundary = @"---------------------------14737809831466499882746641449";
        NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@",boundary];
        [request addValue:contentType forHTTPHeaderField: @"Content-Type"];
        
        NSMutableData *body = [NSMutableData data];
        //        NSDictionary *dict =[self getdataFromPlist];
        [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"user_id\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[userID  dataUsingEncoding:NSUTF8StringEncoding]];
        
        [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"email\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[email_id dataUsingEncoding:NSUTF8StringEncoding]];
        
        [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        NSString *filename1=[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"userfile\"; filename=\"%@\"\r\n",self.fileName];
        [body appendData:[filename1 dataUsingEncoding:NSUTF8StringEncoding]];
        
        [body appendData:[@"Content-Type: application/octet-stream\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[NSData dataWithData:textData]];
        [body appendData:[[NSString stringWithFormat:@"\r\n--%@--\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [request setHTTPBody:body];
        /*NSData *returnData = [NSURLConnection sendSynchronousRequest:request returningResponse:nil error:nil];    [[NSOperationQueue mainQueue] addOperationWithBlock: ^ {
         NSMutableDictionary *dict=(NSMutableDictionary *)[NSJSONSerialization JSONObjectWithData:returnData options:0 error:nil];
         BOOL success=[[dict objectForKey:@"success"]boolValue];
         //NSLog(@"dict %@",dict);
         if (success==1) {
         [SVProgressHUD dismiss];
         
         ViewBackupViewController *viewController = [[ViewBackupViewController alloc]initWithNibName:@"ViewBackupViewController" bundle:nil];
         [self.navigationController pushViewController:viewController animated:YES];
         }
         }];*/
        NSURLSession *session = [NSURLSession sessionWithConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
        
        [[session dataTaskWithRequest:request completionHandler:^(NSData *datas, NSURLResponse *response, NSError *error) {
            
            if(error){
                NSLog(@"dataTaskWithRequest error: %@", error);
                completion(2, nil);
            }else if(datas){
                NSLog(@"data object came Nil");
                completion(3, nil);
            }else{
                NSError *err;
                NSMutableDictionary *dict = [NSJSONSerialization JSONObjectWithData:datas options:NSJSONReadingAllowFragments error:&err];
                BOOL success=[[dict objectForKey:@"success"]boolValue];
                
                if(success){
                    completion(4, dict);
                }
            }
        }] resume];
    }else{
        return;
    }
}

@end




/* // not in use
 
 
 #pragma mark - Addressbook.framework method
 - (NSMutableArray *)getAddressBookAuthorizationFromUser{ //access contacts using AddressBook.framework
 
 NSMutableArray *finalContactList = [[NSMutableArray alloc] init]; //init a array
 
 ABAddressBookRef addressBookRef = ABAddressBookCreateWithOptions(NULL, NULL); //init a addressbook ref object
 
 //Check a AddressBook.framework to access a contacts app
 switch (ABAddressBookGetAuthorizationStatus()) {
 
 case kABAuthorizationStatusNotDetermined:{ //Address book status not determined.
 
 ABAddressBookRequestAccessWithCompletion(addressBookRef, ^(bool granted, CFErrorRef error) {
 if (granted) { // First time access has been granted, add the contact
 
 [finalContactList addObject:[self fetchContactsFromAddressBookFrameWork]];  //fetch all contacts and add to array.
 } else { // User denied to access a contacts app
 
 // Display an alert telling user the contact could not be added
 [self getPermissionToUser];  //Ask permission to access a contacts
 }
 });
 
 }
 break;
 case kABAuthorizationStatusAuthorized:{ //Address book status Authorized.
 
 // The user has previously given access, add the contact
 finalContactList = [self fetchContactsFromAddressBookFrameWork];
 
 }break;
 default:{ //else ask permission to user
 // The user has previously denied access
 // Send an alert telling user to change privacy setting in settings app
 
 [self getPermissionToUser];  //Ask permission to access a contacts
 
 }break;
 }
 return finalContactList;
 }
 
 #pragma mark fetch contacts using addressbook framework
 - (NSMutableArray *)fetchContactsFromAddressBookFrameWork { //fetch contacts using addressbook framework
 
 NSMutableArray *newContactArray  = [NSMutableArray array]; //init a array
 addressBook = ABAddressBookCreateWithOptions(NULL, NULL); //init a addressbook object
 
 arrayOfAllPeople = (__bridge NSArray *) ABAddressBookCopyArrayOfAllPeople(addressBook); //get all contacts from contacts app.
 
 NSUInteger peopleCounter = 0; //set a initial value as ZERO.
 
 //Create custom Contacts list
 for (peopleCounter = 0; peopleCounter < [arrayOfAllPeople count]; peopleCounter++) {
 
 ABRecordRef thisPerson = (__bridge ABRecordRef) [arrayOfAllPeople objectAtIndex:peopleCounter]; // get every person record one by one.
 
 
 ABMultiValueRef phones = ABRecordCopyValue(thisPerson, kABPersonPhoneProperty);
 CFIndex phoneNumberCount = ABMultiValueGetCount(phones);
 NSString *phone;
 NSMutableArray *arry_phone=[[NSMutableArray alloc]init];
 if (phoneNumberCount > 0) {
 for(CFIndex i=0;i<phoneNumberCount  ;i++)
 {
 phone = CFBridgingRelease(ABMultiValueCopyValueAtIndex(phones, i));
 phone= [[phone componentsSeparatedByCharactersInSet:[[NSCharacterSet decimalDigitCharacterSet] invertedSet]] componentsJoinedByString:@""];
 [arry_phone addObject:phone];
 }
 }
 
 NSString *string = [NSString stringWithFormat:@"%@", arry_phone];
 NSString *specialCharacterString = @"*#";
 NSCharacterSet *specialCharacterSet = [NSCharacterSet  characterSetWithCharactersInString:specialCharacterString];
 
 if (![string.lowercaseString rangeOfCharacterFromSet:specialCharacterSet].length) {
 NSMutableDictionary *contantDic = [NSMutableDictionary dictionary]; //init a dictionary
 
 NSString *firstName = CFBridgingRelease(ABRecordCopyValue(thisPerson, kABPersonFirstNameProperty));
 NSString *lastName  = CFBridgingRelease(ABRecordCopyValue(thisPerson, kABPersonLastNameProperty));
 firstName=[self stringFormating:firstName];
 lastName=[self stringFormating:lastName];
 
 if (firstName) {
 
 contantDic[@"first_name"] = firstName;
 }else{
 
 firstName=@"";
 contantDic[@"first_name"] = firstName;
 
 }
 if (lastName) {
 contantDic[@"last_name"]  = lastName;
 }else{
 lastName=@"";
 contantDic[@"last_name"]  = lastName;
 
 }
 NSString *name = [[NSString stringWithFormat:@"%@ %@",firstName, lastName] stringByTrimmingCharactersInSet: [NSCharacterSet symbolCharacterSet]];
 name=[self stringFormating:name];
 contantDic[@"name"] = name ;
 if ([arry_phone count]!=0) {
 contantDic[@"u_phone"] = arry_phone;
 }
 else{
 contantDic[@"u_phone"] = arry_phone;
 }
 
 ABMutableMultiValueRef emails = ABRecordCopyValue(thisPerson, kABPersonEmailProperty);
 CFIndex emailCount = ABMultiValueGetCount(emails);
 NSString *email;
 NSMutableArray *arry_email=[[NSMutableArray alloc]init];
 if (emailCount > 0) {
 for(CFIndex i=0;i<emailCount  ;i++)
 {
 email = CFBridgingRelease(ABMultiValueCopyValueAtIndex(emails, i));
 [arry_email addObject:email];
 }
 
 if ([arry_email count]!=0) {
 contantDic[@"u_email"] = arry_email;
 }
 else{
 contantDic[@"u_email"] = arry_email;
 }
 }
 
 
 ABMultiValueRef addressRef = ABRecordCopyValue(thisPerson, kABPersonAddressProperty);
 if (ABMultiValueGetCount(addressRef) > 0) {
 CFArrayRef allAddresses = ABMultiValueCopyArrayOfAllValues(addressRef);
 CFDictionaryRef addressDict = CFArrayGetValueAtIndex(allAddresses, 0);
 
 NSString *city =CFDictionaryGetValue(addressDict, kABPersonAddressCityKey);
 NSString * street = CFDictionaryGetValue(addressDict, kABPersonAddressStreetKey);
 NSString * postalCode =CFDictionaryGetValue(addressDict, kABPersonAddressZIPKey);
 NSString * country = CFDictionaryGetValue(addressDict, kABPersonAddressCountryKey);
 NSString * countryCode = CFDictionaryGetValue(addressDict, kABPersonAddressCountryCodeKey);
 NSString *state_name = CFDictionaryGetValue(addressDict, kABPersonAddressStateKey);
 NSString *add1 = [NSString stringWithFormat:@"%@ %@",country,countryCode];
 
 contantDic[@"city"] = city;
 contantDic[@"address1"] = street;
 contantDic[@"address2"] = add1;
 contantDic[@"u_zipcode"] = postalCode;
 contantDic[@"state_name"] = state_name;
 CFRelease(allAddresses);
 }
 else
 {
 contantDic[@"city"] = @"";
 contantDic[@"address1"] = @"";
 contantDic[@"address2"] = @"";
 contantDic[@"u_zipcode"] = @"";
 contantDic[@"state_name"] = @"";
 }
 CFRelease(addressRef);
 
 NSString *note  = CFBridgingRelease(ABRecordCopyValue(thisPerson, kABPersonNoteProperty));
 note=[self stringFormating:note];
 if (note.length ==0 || note == nil) {
 note = @"";
 }
 
 NSString *organiztnName  = CFBridgingRelease(ABRecordCopyValue(thisPerson, kABPersonLastNameProperty));
 if (organiztnName.length ==0 || organiztnName == nil) {
 organiztnName = @"";
 }
 
 contantDic[@"company"] = organiztnName;
 contantDic[@"note"] = note;
 
 //            [contantDic setValue:@"NO" forKey:@"selected"]; // add this option for developer usage
 
 //            NSData *contactImageData = (__bridge NSData *)ABPersonCopyImageDataWithFormat(thisPerson, kABPersonImageFormatThumbnail);
 //            NSString *image_str = [contactImageData base64EncodedStringWithOptions:0];
 //            //check and add a person image
 //            if (contactImageData!=nil) {
 //                [contantDic setObject:image_str forKey:@"u_image"];
 //            } else {
 //                [contantDic setObject:@"" forKey:@"u_image"];
 //            }
 
 [newContactArray addObject:contantDic]; //add every person to array
 //            }
 //        }
 }
 }
 
 return newContactArray; //return a contacts
 }

 */
