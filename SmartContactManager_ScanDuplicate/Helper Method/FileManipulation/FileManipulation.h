//
//  FileManipulation.h
//  SmartContactManager_Backup
//
//  Created by 8_Sandhya on 12/09/18.
//  Copyright © 2018 Techathalon. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FileManipulation : NSObject
+(id)sharedContacts;

- (void)writeWholePhoneContactToJsonFile:(NSMutableArray *)arr andFileName:(NSString *)filename;
-(NSDictionary*)readWholePhoneContactDictionaryFromJsonFile:(NSString *)filename;
- (void)clearWholePhoneContactToJsonFile:(NSString *)filename;
- (void)writeRecoverPhoneContactToJsonFile:(NSMutableArray *)arr andFileName:(NSString *)filename;
@end
