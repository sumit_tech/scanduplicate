//
//  FileManipulation.m
//  SmartContactManager_Backup
//
//  Created by 8_Sandhya on 12/09/18.
//  Copyright © 2018 Techathalon. All rights reserved.
//

#import "FileManipulation.h"

@implementation FileManipulation
+ (id)sharedContacts { //Shared instance method
    
    static FileManipulation *sharedMyContacts = nil; //create contactsList Object
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{ //for first time create shared instance object
        sharedMyContacts = [[self alloc] init];
    });
    
    return sharedMyContacts;
}

///whole iphone contacts from CNCONtact store
- (void)writeWholePhoneContactToJsonFile:(NSMutableArray *)arr andFileName:(NSString *)filename {
    NSMutableArray *arr_temp = arr;
    [arr_temp enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        NSMutableDictionary *newdict = [[arr_temp objectAtIndex:idx] mutableCopy];
        [newdict removeObjectForKey:@"contact"];
        [arr_temp replaceObjectAtIndex:idx withObject:newdict];
    }];
    NSDictionary *dict = [[NSDictionary alloc]initWithObjectsAndKeys:[arr_temp copy],@"PhoneContacts", nil];
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
        NSError * err;
        NSData * jsonData = [NSJSONSerialization  dataWithJSONObject:dict options:0 error:&err];
        NSString * aString = [[NSString alloc] initWithData:jsonData  encoding:NSUTF8StringEncoding];
        
        // Build the path, and create if needed.
        NSString* filePath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
        NSString* fileName = filename;
        NSString* fileAtPath = [filePath stringByAppendingPathComponent:fileName];
        
        if (![[NSFileManager defaultManager] fileExistsAtPath:fileAtPath]) {
            [[NSFileManager defaultManager] createFileAtPath:fileAtPath contents:nil attributes:nil];
        }
        
        // The main act...
        [[aString dataUsingEncoding:NSUTF8StringEncoding] writeToFile:fileAtPath atomically:NO];
    });
}
- (void)writeRecoverPhoneContactToJsonFile:(NSMutableArray *)arr andFileName:(NSString *)filename {
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
        NSMutableArray *arr_temp = arr;
        NSDictionary *dict = [[NSDictionary alloc]initWithObjectsAndKeys:[arr_temp copy],@"PhoneContacts", nil];
        
        NSError * err;
        NSData * jsonData = [NSJSONSerialization  dataWithJSONObject:dict options:0 error:&err];
        NSString * aString = [[NSString alloc] initWithData:jsonData  encoding:NSUTF8StringEncoding];
        
        // Build the path, and create if needed.
        NSString* filePath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
        NSString* fileAtPath = [filePath stringByAppendingPathComponent:filename];
        
        if (![[NSFileManager defaultManager] fileExistsAtPath:fileAtPath]) {
            [[NSFileManager defaultManager] createFileAtPath:fileAtPath contents:nil attributes:nil];
        }
        
        // The main act...
        [[aString dataUsingEncoding:NSUTF8StringEncoding] writeToFile:fileAtPath atomically:NO];
    });
}
-(NSDictionary*)readWholePhoneContactDictionaryFromJsonFile:(NSString *)filename {
    
    // Build the path...
    NSString* filePath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString* fileName = filename;
    NSString* fileAtPath = [filePath stringByAppendingPathComponent:fileName];
    NSString *jsonString =[[NSString alloc] initWithData:[NSData dataWithContentsOfFile:fileAtPath] encoding:NSUTF8StringEncoding];
    
    //string to Nsdictionary
    NSData *data = [jsonString dataUsingEncoding:NSUTF8StringEncoding];
    NSDictionary * json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
    // The main act...
    return json;
}
- (void)clearWholePhoneContactToJsonFile:(NSString *)filename {
    
    NSDictionary *dict = [[NSDictionary alloc]init];
    NSError * err;
    NSData * jsonData = [NSJSONSerialization  dataWithJSONObject:dict options:0 error:&err];
    NSString * aString = [[NSString alloc] initWithData:jsonData  encoding:NSUTF8StringEncoding];
    
    // Build the path, and create if needed.
    NSString* filePath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString* fileName = filename;
    NSString* fileAtPath = [filePath stringByAppendingPathComponent:fileName];
    
    if (![[NSFileManager defaultManager] fileExistsAtPath:fileAtPath]) {
        [[NSFileManager defaultManager] createFileAtPath:fileAtPath contents:nil attributes:nil];
    }
    
    // The main act...
    [[aString dataUsingEncoding:NSUTF8StringEncoding] writeToFile:fileAtPath atomically:NO];
}
@end
