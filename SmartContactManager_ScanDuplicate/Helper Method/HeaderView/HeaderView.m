//
//  HeaderView.m
//  SmartContactManager_Backup
//
//  Created by 8_Sandhya on 27/08/18.
//  Copyright © 2018 Techathalon. All rights reserved.
//

#import "HeaderView.h"
#import "MasterVC.h"
@interface HeaderView ()
@property (strong, nonatomic) IBOutlet UIImageView *img_leftOption;
@property (strong, nonatomic) IBOutlet UIImageView *img_rightOption;
@property (strong, nonatomic) IBOutlet UIView *view_left;
@property (strong, nonatomic) IBOutlet UIView *view_right;
@property (strong, nonatomic) IBOutlet UIImageView *imageHeader;

- (IBAction)btn_leftClick:(id)sender;
- (IBAction)btn_rightClick:(id)sender;


@end

@implementation HeaderView

-(instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if(self)
    {
        [self customInit];
    }
    return self;
}
-(instancetype)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if(self)
    {
        [self customInit];
    }
    return self;
}

-(void)customInit
{
    UIView *view=[[[NSBundle bundleForClass:[self class]] loadNibNamed:NSStringFromClass([self class]) owner:self options:nil] firstObject];
    [self addSubview:view];
    CGRect frame=self.bounds;
    view.frame=frame;
    [self layoutIfNeeded];
    [self setNeedsLayout];
    [self.img_leftOption setImage:[UIImage imageNamed:@"icon_delete"]];

}

- (IBAction)btn_leftClick:(id)sender {
    if([self.delegate respondsToSelector:@selector(btnLeftClicked)])
    {
        [self.delegate btnLeftClicked];
    }
}

- (IBAction)btn_rightClick:(id)sender {
    if([self.delegate respondsToSelector:@selector(btnRigtClicked)])
    {
        [self.delegate btnRigtClicked];
    }
}

#pragma mark - helper Method
-(void)setTitle:(NSString *)title
{
    _lbl_title.text = title;
}

-(void)setTitleColor:(UIColor *)color
{
    _lbl_title.textColor = color;
}

-(void)changeBackgroundColor:(UIColor *)color
{
//    [self setBackgroundColor:color];
    [self.imageHeader setImage:[UIImage imageNamed:@"blurBGhead"]];
}

-(void)changeLeftIconImage:(UIImage *)image
{
    [self.img_leftOption setImage:image];
}

-(void)changeRightIconImage:(UIImage *)image
{
    [self.view_right setHidden:NO];
    [self.lbl_right setHidden:YES];
    [self.img_rightOption setImage:image];
}

-(void)changeRightTitle:(NSString *)str withColor:(UIColor *)color
{
    [self changeRightTitle:str];
    [self.lbl_right setTextColor:color];
}
-(void)changeRightTitle:(NSString *)str
{
    [self.view_right setHidden:NO];
    [self.img_rightOption setHidden:YES];
    [self.lbl_right setHidden:NO];
    [self.lbl_right setText:str];
}
-(void)setRightColor:(UIColor *)rightColor
{
    [self.view_right setBackgroundColor:rightColor];
}
-(void)hideViewLeft{
    [self.view_left setHidden:YES];
}
@end
