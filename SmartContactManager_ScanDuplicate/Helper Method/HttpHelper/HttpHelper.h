//
//  HttpHelper.h
//  JSonDemo
//

//  Copyright (c) 2012 dharni. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AppDelegate.h"

@class MainViewController;

@protocol HttpHelperDelegate <NSObject>
@required
-(void)setBackApiResponse:(NSMutableDictionary *)response api_number:(int)api_number;
@optional
-(void) setServerTimeOut:(int)api_number;
-(void)responseError:(NSError *)error api_number:(int)api_number;
@end
@interface HttpHelper : NSObject<NSURLConnectionDataDelegate,NSURLConnectionDelegate>{        
    AppDelegate *app;
    NSMutableData *data;    
    NSMutableDictionary *dict;   
    bool startloading;
    NSMutableString *xmldata;
    NSURLConnection *urlConnection;
    int api_number;
    
}


@property (nonatomic,retain) NSMutableDictionary *dict;
@property(nonatomic,retain)NSMutableData *data;
@property(nonatomic,assign)bool startloading;
@property(retain,nonatomic) NSMutableString *xmldata;
@property(weak,nonatomic)id<HttpHelperDelegate>delegate;
-(id) initWithMUrl:(NSString *) serverurl Dictionary:(NSDictionary *) valuesPairs isloading:(bool)loading ;
-(id) initWithMOnlyUrl:(NSString *)serverurl isloading:(bool)loading;

-(id) initWithMUrl:(NSString *) serverurl Dictionary:(NSDictionary *) valuesPairs isloading:(bool)loading api_number:(int)request_api_number message:(NSString *)message;
-(id) initWithMOnlyUrl:(NSString *)serverurl isloading:(bool)loading api_number:(int)request_api_number message:(NSString *)message;

@end
