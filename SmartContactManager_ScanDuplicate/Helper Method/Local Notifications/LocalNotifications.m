//
//  LocalNotifications.m
//  SmartContactManager_Backup
//
//  Created by 8_Sandhya on 14/09/18.
//  Copyright © 2018 Techathalon. All rights reserved.
//
#import <UIKit/UIKit.h>
#import "LocalNotifications.h"
#import <UserNotifications/UserNotifications.h>
#import "PlistHandler.h"
@implementation LocalNotifications
+ (id)sharedObject {
    static LocalNotifications *sharedMyViews = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{ //for first time create shared instance object
        sharedMyViews = [[self alloc] init];
    });
    return sharedMyViews;
}

-(void)resetLocalNotification
{
    UNUserNotificationCenter *center = [UNUserNotificationCenter currentNotificationCenter];
    [center removeAllPendingNotificationRequests];
    NSString *reminderSelected = [[PlistHandler sharedObject]getValueFromPlistByKey:@"reminderSelect"];
    UNMutableNotificationContent *objNotificationContent = [[UNMutableNotificationContent alloc] init];
    objNotificationContent.title = [NSString localizedUserNotificationStringForKey:@"Notification!" arguments:nil];
    objNotificationContent.body = [NSString localizedUserNotificationStringForKey:@"Hey Backup time !"
                                                                        arguments:nil];
    objNotificationContent.sound = [UNNotificationSound defaultSound];
    objNotificationContent.badge = @([[UIApplication sharedApplication] applicationIconBadgeNumber] + 1);
    UNTimeIntervalNotificationTrigger *trigger;
    switch ([reminderSelected integerValue]) {
        case 1: // 5   432000.f
            trigger = [UNTimeIntervalNotificationTrigger triggerWithTimeInterval:432000.f repeats:YES];
            break;
        case 2: // 10   864000.f
            trigger = [UNTimeIntervalNotificationTrigger triggerWithTimeInterval:864000.f repeats:YES];
            break;
        case 3: // 15   1296000.f
            trigger = [UNTimeIntervalNotificationTrigger triggerWithTimeInterval:1296000.f repeats:YES];
            break;
        case 4: // 30  2592000.f
            trigger = [UNTimeIntervalNotificationTrigger triggerWithTimeInterval:2592000.f repeats:YES];
            break;
        default:
            break;
    }
    UNNotificationRequest *request = [UNNotificationRequest requestWithIdentifier:@"ten" content:objNotificationContent trigger:trigger];
    [center addNotificationRequest:request withCompletionHandler:^(NSError * _Nullable error) {
        if (!error) {
            NSLog(@"Local Notification succeeded");
        }
        else {
            NSLog(@"Local Notification failed");
        }
    }];
}
@end
