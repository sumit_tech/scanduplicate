//
//  LoginPopUpView.m
//  SmartContactManager_Backup
//
//  Created by 8_Sandhya on 17/11/18.
//  Copyright © 2018 Techathalon. All rights reserved.
//

#import "LoginPopUpView.h"
#import "MasterVC.h"
@interface LoginPopUpView()

@property (strong, nonatomic) IBOutlet UIView *view_innerGP;

@end
@implementation LoginPopUpView

-(instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if(self)
    {
        [self customInit];
    }
    return self;
}
-(instancetype)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if(self)
    {
        [self customInit];
    }
    return self;
}

-(void)customInit
{
    UIView *view=[[[NSBundle bundleForClass:[self class]] loadNibNamed:NSStringFromClass([self class]) owner:self options:nil] firstObject];
    [self addSubview:view];
    CGRect frame=self.bounds;
    view.frame=frame;
    [self layoutIfNeeded];
    [self setNeedsLayout];
    UIToolbar* numberToolbar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, 320, 50)];
    numberToolbar.barStyle = UIBarStyleBlackTranslucent;
    numberToolbar.items = @[[[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
                            [[UIBarButtonItem alloc]initWithTitle:@"Apply" style:UIBarButtonItemStyleDone target:self action:@selector(doneWithNumberPad)]];
    [numberToolbar sizeToFit];
    self.txt_newPass.inputAccessoryView = numberToolbar;
    self.txt_newPass.delegate = self;
    self.txt_cnfpass.inputAccessoryView = numberToolbar;
    self.txt_cnfpass.delegate = self;
    self.txt_email.delegate = self;
    [_view_signIn.layer setCornerRadius:7];
    [_view_fb.layer setCornerRadius:_view_fb.bounds.size.height/2];
    [_view_gp.layer setCornerRadius:_view_gp.bounds.size.height/2];
    [GIDSignInButton class];
    GIDSignIn *signIn = [GIDSignIn sharedInstance];
    signIn.shouldFetchBasicProfile = YES;
    [[viewModification sharedViews] changeViewtoCornerRadiusWithShadow:8 AndBorder:0.5 andBorderColor:[UIColor whiteColor] toView:self.view_gp andShadowRadius:2.0 andShadowOpacity:0.8];
    [[viewModification sharedViews] changeViewtoCornerRadiusWithShadow:8 AndBorder:0.5 andBorderColor:[UIColor whiteColor] toView:self.view_fb andShadowRadius:2.0 andShadowOpacity:0.8];
    [[viewModification sharedViews] changeViewtoCornerRadiusWithShadow:8 AndBorder:0.5 andBorderColor:[UIColor whiteColor] toView:self.view_signIn andShadowRadius:2.0 andShadowOpacity:0.8];
}

#pragma mark - IBAction
- (IBAction)btn_fb:(id)sender {
    [self endEditing:YES];

    FBSDKLoginManager *login = [[FBSDKLoginManager alloc] init];
    [login logInWithReadPermissions:@[@"public_profile", @"email"] fromViewController:self.parentVC handler:^(FBSDKLoginManagerLoginResult *result, NSError *error) {
        
        if (error)
        {
            NSLog(@"Unexpected login error: %@", error);
            NSString *alertMessage = error.userInfo[FBSDKErrorLocalizedDescriptionKey] ?: @"There was a problem logging in. Please try again later.";
            NSString *alertTitle = error.userInfo[FBSDKErrorLocalizedTitleKey] ?: @"Oops";
            [[AlertControllerDesign sharedObject]alertController:alertTitle withMessage:alertMessage andButtonOne:@"Ok" andButtontwo:@"" WithCompletionHandler:^(BOOL completed) {
                
            }];
//            [[[UIAlertView alloc] initWithTitle:alertTitle
//                                        message:alertMessage
//                                       delegate:nil
//                              cancelButtonTitle:@"OK"
//                              otherButtonTitles:nil] show];
        }
        else
        {
            if(result.token)   // This means if There is current access token.
            {
                [[[FBSDKGraphRequest alloc] initWithGraphPath:@"me"
                                                   parameters:@{@"fields": @"picture, name, email"}]
                 startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id userinfo, NSError *error) {
                     if (!error) {
                         
                         dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0ul);
                         dispatch_async(queue, ^(void) {
                             
                             dispatch_async(dispatch_get_main_queue(), ^{
                                 NSLog(@"user info: %@", userinfo);
                                 NSLog(@"%@",userinfo[@"email"]);
                                 
                             });
                         });
                     }
                     else{
                         NSLog(@"%@", [error localizedDescription]);
                     }
                 }];
            }
            NSLog(@"Login Cancel");
        }
    }];
}
- (IBAction)btn_gp:(id)sender {
    [self endEditing:YES];

        if([self.delegate respondsToSelector:@selector(btn_GoogleLogin)])
        [self.delegate btn_GoogleLogin];
}

- (IBAction)btn_SignIn:(id)sender {
    [self endEditing:YES];
    if(_txt_newPass.text.length == 0){
        [self.txt_email setTextWithEmailValidation:(ACFloatingTextField *)self.txt_email WithCompletionHandler:^(BOOL completed) {
            if(completed && [self.delegate respondsToSelector:@selector(signInClick:)])
                [self.delegate signInClick:self.txt_email.text];
        }];
    }else{
        if([self.txt_cnfpass.text isEqualToString:self.txt_newPass.text] && !(self.txt_newPass.text.length < 4) && self.txt_email.text.length > 0){
            if([self.delegate respondsToSelector:@selector(signInWithPassword:withEmail:)])
                [self.delegate signInWithPassword:self.txt_newPass.text withEmail:self.txt_email.text];
        }
        else{
            [self.txt_cnfpass showErrorWithText:@"Password doesn't match"];
            [self.txt_newPass showErrorWithText:@"Password doesn't match"];
        }
    }
}

- (IBAction)btnSkip:(id)sender {
    [self endEditing:YES];

    if([self.delegate respondsToSelector:@selector(btn_skip)])
        [self.delegate btn_skip];
}

#pragma mark - HelperMethod

-(void)viewWithOutPassword
{
    [self.view_newPass setHidden:YES];
    [self.view_cnfPass setHidden:YES];
    [self.cnst_passHeight setConstant:0];
    [self.cnst_cnfHeight setConstant:0];
//    [self.cnst_newHeifht setConstant:0];
    [self layoutIfNeeded];
}

-(void)viewWithPassword
{
    CGRect fr = self.frame;
    fr.size.height = self.frame.size.height + 102;
    self.frame = fr;
    [self.view_newPass setHidden:NO];
    [self.view_cnfPass setHidden:NO];
    [self.cnst_passHeight setConstant:45];
    [self.cnst_cnfHeight setConstant:12];
//    [self.cnst_newHeifht setConstant:12];

    [UIView animateWithDuration:0.5 animations:^{
        [self layoutIfNeeded];
        [self layoutSubviews];
    }];
    
}

-(void)viewWithPassword:(NSString *)email
{
    CGRect fr = self.frame;
    fr.size.height = self.frame.size.height + 102;
    self.frame = fr;
    [self.view_newPass setHidden:NO];
    [self.view_cnfPass setHidden:NO];
    [self.cnst_passHeight setConstant:45];
    [self.cnst_cnfHeight setConstant:12];
    self.txt_email.text = email;
    //    [self.cnst_newHeifht setConstant:12];
    
    [UIView animateWithDuration:0.5 animations:^{
        [self layoutIfNeeded];
        [self layoutSubviews];
    }];
    
}
-(void)doneWithNumberPad
{
    [self endEditing:YES];
}
#pragma mark - UITextFieldDelegate
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [self endEditing:YES];
    return YES;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    if(textField == self.txt_newPass || textField == self.txt_cnfpass){
        ACFloatingTextField* textField1 = (ACFloatingTextField*)textField;
        if(range.length + range.location > textField.text.length)
            return NO;
        
        NSUInteger newLength = [textField.text length] + [string length] - range.length;
        BOOL isCount = newLength <= 4;
        if(!isCount)
            [textField1 showErrorWithText:@"Only 4 character"];
        return newLength <= 4;
    }
    return YES;
}

@end
