//
//  MergeAlert.h
//  SmartContactManager_ScanDuplicate
//
//  Created by 8_Sandhya on 29/12/18.
//  Copyright © 2018 Techathalon. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@protocol MergeAlertDelegate <NSObject>
@optional
-(void)btn_YesAlert:(NSIndexPath *)index;
-(void)btn_CancelAlert;
@end


@interface MergeAlert : UIView
@property (strong, nonatomic) NSDictionary *dict_contact;
@property (strong, nonatomic) NSString *key;
@property (strong, nonatomic) NSIndexPath *index;

@property(weak,nonatomic)id <MergeAlertDelegate> delegate;
-(void)callReloadCell:(NSDictionary*)dict andKey:(NSString *)key;

@end

NS_ASSUME_NONNULL_END
