//
//  MergePopUp.m
//  SmartContactManager_ScanDuplicate
//
//  Created by 8_Sandhya on 21/11/18.
//  Copyright © 2018 Techathalon. All rights reserved.
//

#import "MergePopUp.h"
#import "DuplicateCell.h"
@interface MergePopUp()<UITableViewDelegate, UITableViewDataSource>
{
     NSArray *arr_color;
}
@end

@implementation MergePopUp

-(instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if(self)
    {
        [self customInit];
    }
    return self;
}
-(instancetype)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if(self)
    {
        [self customInit];
    }
    return self;
}

-(void)customInit
{
    UIView *view=[[[NSBundle bundleForClass:[self class]] loadNibNamed:NSStringFromClass([self class]) owner:self options:nil] firstObject];
    [self addSubview:view];
    CGRect frame=self.bounds;
    view.frame=frame;
    [self layoutIfNeeded];
    [self setNeedsLayout];
    [self.tableview registerNib:[UINib nibWithNibName:@"DuplicateCell" bundle:nil] forCellReuseIdentifier:@"CellID"];
    _tableview.delegate = self;
    self.tableview.dataSource = self;
    arr_color = [[NSArray alloc]initWithObjects:[UIColor blackColor], UIColor.blueColor, UIColor.brownColor, UIColor.cyanColor, UIColor.greenColor, UIColor.magentaColor, UIColor.orangeColor,UIColor.purpleColor, UIColor.redColor, UIColor.yellowColor,UIColor.grayColor, nil];

}
- (IBAction)btn_Ok:(id)sender {
    if([self.delegate respondsToSelector:@selector(hideMergePopup)])
        [self.delegate hideMergePopup];
}

-(void)reloadCell:(NSMutableArray  *)arr
{
    _arr_display = nil;
    _arr_display = [[NSMutableArray alloc]initWithArray:[arr copy] copyItems:YES];
//    _arr_display = arr;
    [self.tableview reloadData];
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return  _arr_display.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    DuplicateCell *cell = (DuplicateCell *)[tableView dequeueReusableCellWithIdentifier:@"CellID"];
    if(!cell)
        cell = [[DuplicateCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"CellID"];
    NSDictionary *dict = [[[_arr_display objectAtIndex:indexPath.row]objectForKey:@"name"]firstObject ];
    NSString *name;
//    = [NSString stringWithFormat:@"%@",[dict objectForKey:@"name"]];
    //    cell.image.image = [UIImage imageNamed:@"icon_contactCard"];
    
    switch (_barItemIndex) {
        case 1:
          dict = [[[_arr_display objectAtIndex:indexPath.row]objectForKey:@"name"]firstObject ];
            break;
        case 2:
            dict = [[[_arr_display objectAtIndex:indexPath.row]objectForKey:@"number"]firstObject ];
            break;
        case 3:
            dict = [[[_arr_display objectAtIndex:indexPath.row]objectForKey:@"email"]firstObject ];
            break;

        default:
            break;
    }
    name= [NSString stringWithFormat:@"%@",[dict objectForKey:@"name"]];

    cell.lbl_name.text = name;
    NSArray *arr = [cell.lbl_name.text componentsSeparatedByString:@" "];
    cell.lbl_detail.text = @"";
    [cell.img_arr setHidden:YES];
    cell.lbl_fill.backgroundColor = (UIColor *)[arr_color objectAtIndex:(indexPath.row%11)];
    if(arr.count > 1)
    {
        NSString *sec = (NSString *)[arr lastObject];
        NSString *str = [NSString stringWithFormat:@"%@%@",[[arr firstObject] substringToIndex:1], (sec.length > 0) ? [sec substringToIndex:1] : @""];
        cell.lbl_initial.text = str;
    }else{
        cell.lbl_initial.text = [cell.lbl_name.text substringToIndex:1];
    }
    [cell retest];
    [cell layoutIfNeeded];
    [cell layoutSubviews];
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 50;
}
- (IBAction)btn_minimize:(id)sender {
    if([self.delegate respondsToSelector:@selector(hideMergePopup)])
        [self.delegate hideMergePopup];
}
@end
