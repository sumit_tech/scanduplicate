//
//  ShareDialogView.h
//  SmartContactManager_Backup
//
//  Created by 8_Sandhya on 08/09/18.
//  Copyright © 2018 Techathalon. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MasterView.h"
@protocol ShareDialogViewDelegate <NSObject>
//@optional
-(void)shareFailed;
-(void)shareSuccess;
@end

@interface ShareDialogView : MasterView
@property(weak,nonatomic)id <ShareDialogViewDelegate> delegate;

-(void)showSideMenu:(ShareDialogView *)view;
@property (strong, nonatomic)UIView *parentView;
@property (strong, nonatomic)UIViewController *parentVC;
@end
