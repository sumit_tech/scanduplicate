//
//  viewModification.h
//  SmartContactManager_Backup
//
//  Created by 8_Sandhya on 27/08/18.
//  Copyright © 2018 Techathalon. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface viewModification : NSObject
-(void)changeViewtoCornerRadius:(int)radius AndBorder:(float)border_width andBorderColor:(UIColor *)color toView:(UIView *)view;
-(void)changeViewtoCornerRadiusWithShadow:(int)radius AndBorder:(float)border_width andBorderColor:(UIColor *)color toView:(UIView *)v andShadowRadius:(int)sRadius andShadowOpacity:(float)sLength;
-(void)changeViewtoCornerRadiusWithShadow:(int)radius AndBorder:(float)border_width andBorderColor:(UIColor *)color toView:(UIView *)v andShadowRadius:(int)sRadius andShadowOpacity:(float)sLength andsetShadowOffset:(CGSize)size;
-(void) setCornerRadius:(int)radius andView:(UIView *)view;
-(void)changeViewtoCornerRadiusInArray:(NSArray *)arr_radius AndBorder:(float)border_width andBorderColor:(UIColor *)color toView:(NSArray *)arr_view;
-(void)custom:(UIView *)outView;
- (void)setBackgroundGradient:(UIView *)mainView color1Red:(float)colorR1 color1Green:(float)colorG1 color1Blue:(float)colorB1 color2Red:(float)colorR2 color2Green:(float)colorG2 color2Blue:(float)colorB2 alpha:(float)alpha;

+(id)sharedViews;
@end
