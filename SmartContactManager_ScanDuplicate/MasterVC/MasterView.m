//
//  MasterView.m
//  SmartContactManager_Backup
//
//  Created by 8_Sandhya on 08/09/18.
//  Copyright © 2018 Techathalon. All rights reserved.
//

#import "MasterView.h"
@implementation MasterView

-(instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if(self)
    {
        [self customInitSuper];
    }
    return  self;
}

-(instancetype)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if(self)
    {
        [self customInitSuper];
    }
    return self;
}

-(void)customInitSuper
{
    app = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    
}

@end
