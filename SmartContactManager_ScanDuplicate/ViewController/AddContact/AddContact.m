//
//  AddContact.m
//  SmartContactManager_Backup
//
//  Created by 8_Sandhya on 30/08/18.
//  Copyright © 2018 Techathalon. All rights reserved.
//

#import "AddContact.h"
#import "HeaderView.h"
#import "ACFloatingTextField.h"
//#import "ContactAction.h"
@interface AddContact ()<HeaderViewDelegate, UITextFieldDelegate>
{
    CGRect activeFrame;
}
@property (strong, nonatomic) IBOutlet HeaderView *view_header;
@property (strong, nonatomic) IBOutlet ACFloatingTextField *txt_firstName;
@property (strong, nonatomic) IBOutlet ACFloatingTextField *txt_middleName;
@property (strong, nonatomic) IBOutlet ACFloatingTextField *txt_lastName;
@property (strong, nonatomic) IBOutlet ACFloatingTextField *txt_phoneNo;
@property (strong, nonatomic) IBOutlet ACFloatingTextField *txt_email;
@property (strong, nonatomic) IBOutlet ACFloatingTextField *txt_street;
@property (strong, nonatomic) IBOutlet ACFloatingTextField *txt_city;
@property (strong, nonatomic) IBOutlet ACFloatingTextField *txt_postcode;
@property (strong, nonatomic) IBOutlet ACFloatingTextField *txt_state;
@property (strong, nonatomic) IBOutlet ACFloatingTextField *txt_country;
@property (strong, nonatomic) IBOutlet UIScrollView *scrollview;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *cnst_viewHeight;

@end

@implementation AddContact

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.view_header setDelegate:self];
//    [self.view_header changeBackgroundColor:Bg_color];
    [self.view_header changeLeftIconImage:[UIImage imageNamed:@"icon_menu"]];
    [self.view_header changeRightTitle:@"Done"];
    [self.view_header setTitle:@"Add Contact"];
    [self performSelector:@selector(shadowHeaderView:) withObject:self.view_header afterDelay:0.5];

    [self.txt_firstName setDelegate:self];
    [self.txt_middleName setDelegate:self];
    [self.txt_lastName setDelegate:self];
    [self.txt_phoneNo setDelegate:self];
    [self.txt_email setDelegate:self];
    [self.txt_street setDelegate:self];
    [self.txt_city setDelegate:self];
    [self.txt_postcode setDelegate:self];
    [self.txt_state setDelegate:self];
    [self.txt_country setDelegate:self];
    // Do any additional setup after loading the view from its nib.
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardDidShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:YES];
    [[NSNotificationCenter defaultCenter] removeObserver:self name: UIKeyboardDidHideNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self                                                    name:UIKeyboardWillHideNotification object:nil];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - HeaderViewDelegate
-(void)btnLeftClicked
{
    [self slideView];
}

-(void)btnRigtClicked
{
    [self.view endEditing:YES];
    if([self checkUserEnterData]){
        NSMutableDictionary *new = [[NSMutableDictionary alloc]init];
        if(self.txt_firstName.text.length > 0)
            [new setObject:self.txt_firstName.text forKey:@"givenName"];
        if(self.txt_middleName.text.length > 0)
            [new setObject:self.txt_firstName.text forKey:@"middleName"];
        if(self.txt_lastName.text.length > 0)
            [new setObject:self.txt_firstName.text forKey:@"familyName"];
        if(self.txt_phoneNo.text.length > 0)
            [new setObject:@[self.txt_phoneNo.text] forKey:@"phoneNumbers"];
        if(self.txt_email.text.length > 0)
            [new setObject:@[self.txt_email.text] forKey:@"emailAddresses"];
        if(self.txt_street.text.length > 0)
            [new setObject:self.txt_firstName.text forKey:@"street"];
        if(self.txt_city.text.length > 0)
            [new setObject:self.txt_firstName.text forKey:@"city"];
        if(self.txt_postcode.text.length > 0)
            [new setObject:self.txt_firstName.text forKey:@"postalCode"];
        if(self.txt_state.text.length > 0)
            [new setObject:self.txt_firstName.text forKey:@"state"];
        if(self.txt_country.text.length > 0)
            [new setObject:self.txt_firstName.text forKey:@"country"];
        [[ContactAction sharedContacts]addNewContactToDevice:[new copy] andSaveDirectly:NO];
    }
}
#pragma mark - UITextFieldDelegate
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [self.view endEditing:YES];
    return YES;
}
-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    activeFrame = textField.frame;
}

- (void)keyboardWasShown:(NSNotification *)notification {
    NSDictionary* info = [notification userInfo];
    CGSize keyboardSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    CGPoint buttonOrigin = activeFrame.origin;
    CGRect visibleRect = self.view.frame;
    visibleRect.size.height -= keyboardSize.height;
    if (!CGRectContainsPoint(visibleRect, buttonOrigin)){
        CGPoint scrollPoint = CGPointMake(0.0, activeFrame.origin.y - 20);
        self.cnst_viewHeight.constant = 544 + keyboardSize.height;
        [self.view layoutIfNeeded];
        [self.scrollview setContentOffset:scrollPoint animated:YES];
    }
}

- (void)keyboardWillBeHidden:(NSNotification *)notification {
    self.cnst_viewHeight.constant = 544;
    [self.view layoutIfNeeded];
    [self.scrollview setContentOffset:CGPointZero animated:YES];
}

-(BOOL)checkUserEnterData
{
    if(self.txt_firstName.text.length > 0 || self.txt_middleName.text.length > 0 ||       self.txt_lastName.text.length > 0 || self.txt_phoneNo.text.length > 0 ||       self.txt_email.text.length > 0 || self.txt_street.text.length > 0 ||       self.txt_city.text.length > 0 || self.txt_postcode.text.length > 0 || self.txt_country.text.length > 0 || self.txt_state.text.length > 0 )
        return YES;
    return NO;
}
@end
