//
//  BackUpCell.h
//  SmartContactManager_Backup
//
//  Created by 8_Sandhya on 29/10/18.
//  Copyright © 2018 Techathalon. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SWTableViewCell.h"
@interface BackUpCell : SWTableViewCell
@property (strong, nonatomic) IBOutlet UILabel *lbl_date;
@property (strong, nonatomic) IBOutlet UILabel *lbl_fileName;
@property (strong, nonatomic) IBOutlet UILabel *lbl_month;

@end
