//
//  ContactMemberList.h
//  SmartContactManager_Backup
//
//  Created by 8_Sandhya on 27/08/18.
//  Copyright © 2018 Techathalon. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MasterVC.h"

@interface ContactMemberList : MasterVC
@property (strong, nonatomic) NSMutableArray *arr_contact;
@property (strong, nonatomic) NSString *pageCameFrom;
@end
