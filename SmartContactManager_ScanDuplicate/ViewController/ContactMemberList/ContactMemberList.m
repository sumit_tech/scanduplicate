//
//  ContactMemberList.m
//  SmartContactManager_Backup
//
//  Created by 8_Sandhya on 27/08/18.
//  Copyright © 2018 Techathalon. All rights reserved.
//

#import "ContactMemberList.h"
#import "HeaderView.h"
#import "ContactCell.h"
#import <ContactsUI/ContactsUI.h>
#import "ViewController.h"
#import "DuplicateCell.h"
//#import "ContactAction.h"
@interface ContactMemberList ()<UITableViewDelegate, UITableViewDataSource, CNContactViewControllerDelegate, HeaderViewDelegate, SWTableViewCellDelegate>
{
    NSMutableArray *array;
    NSArray *arr_color;
    BOOL isHint;
}
@property (strong, nonatomic) IBOutlet HeaderView *view_header;
@property (strong, nonatomic) IBOutlet UITableView *tableview;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *btn_deleteheight;
@property (strong, nonatomic) IBOutlet UIButton *btn_delete;

@end

@implementation ContactMemberList

- (void)viewDidLoad {
    [super viewDidLoad];
//    [_view_header changeBackgroundColor:Bg_color];
    [self.view_header changeLeftIconImage:[UIImage imageNamed:@"icon_menu"]];
    _view_header.delegate = self;
    [self.tableview registerNib:[UINib nibWithNibName:@"ContactCell" bundle:nil] forCellReuseIdentifier:CONTACTCELL_ID];
    [self.tableview registerNib:[UINib nibWithNibName:@"DuplicateCell" bundle:nil] forCellReuseIdentifier:@"CellID"];
    arr_color = [[NSArray alloc]initWithObjects:[UIColor blackColor], UIColor.blueColor, UIColor.brownColor, UIColor.cyanColor, UIColor.greenColor, UIColor.magentaColor, UIColor.orangeColor, UIColor.purpleColor, UIColor.redColor, UIColor.yellowColor, UIColor.grayColor, nil];
    isHint = YES;
    self.tableview.delegate = self;
    self.tableview.dataSource = self;
    [self performSelector:@selector(shadowHeaderView:) withObject:self.view_header afterDelay:0.5];
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
  
//    NSSortDescriptor *sortByName = [NSSortDescriptor sortDescriptorWithKey:@"name" ascending:YES];
//    NSArray *sortDescriptors = [NSArray arrayWithObject:sortByName];
//    self.arr_contact = [[[ self.arr_contact copy] sortedArrayUsingDescriptors:sortDescriptors] mutableCopy];
    [self.view_header setTitle:_pageCameFrom];
//            [self addKeySelected:@"0"];

    [[ContactList sharedContacts]fetchAllContacts:NO WithCompletionHandler:^(int result, NSMutableArray *arr) {
        if(result == 2){
            self.arr_contact = arr;
            [self addKeySelected:@"0"];
            
            NSSortDescriptor *sortByName = [NSSortDescriptor sortDescriptorWithKey:@"name"
                                                                         ascending:YES];
            NSArray *sortDescriptors = [NSArray arrayWithObject:sortByName];
            NSArray *sortedArray = [self.arr_contact sortedArrayUsingDescriptors:sortDescriptors];
            self.arr_contact = [sortedArray mutableCopy];
            dispatch_async(dispatch_get_main_queue(), ^{
                [self.tableview reloadData];
            });
        }
    }];
    [self.tableview reloadData];
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:YES];
    //delete
    if([self.pageCameFrom isEqualToString:@"Delete Contact"]){
        _btn_deleteheight.constant = 50;
        [_btn_delete setHidden:NO];
        [self.view_header changeRightTitle:@"All"];
        [self setBackgroundGradient:_btn_delete color1Red:223.0 color1Green:89.0 color1Blue:108.0 color2Red:255.0 color2Green:178.0 color2Blue:57.0 alpha:1.0];
        [self.view layoutIfNeeded];
    }else{
        _btn_deleteheight.constant = 0;
        [self.view layoutIfNeeded];
    }
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}
-(void)addKeySelected:(NSString *)Value{
    [_arr_contact enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        NSMutableDictionary *dict = [[self.arr_contact objectAtIndex:idx] mutableCopy];
        [dict setObject:Value forKey:@"isSelect"];
        [self.arr_contact replaceObjectAtIndex:idx withObject:dict];
    }];
}
- (IBAction)btn_back:(id)sender {
    [app.navController popViewControllerAnimated:YES];
}
- (NSArray *)deleteButtons
{
    NSMutableArray *rightUtilityButtons = [NSMutableArray new];
    [rightUtilityButtons sw_addUtilityButtonWithColor : [UIColor colorWithRed:233/255.0f green:102/255.0f blue:65/255.0f alpha:1.0] title:@"Delete" Icon:[UIImage imageNamed:@"icon_cross"]];
    return rightUtilityButtons;
}

#pragma mark - tableview delegate Method
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _arr_contact.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    DuplicateCell *cell = (DuplicateCell *)[tableView dequeueReusableCellWithIdentifier:@"CellID"];
    if(!cell)
        cell = [[DuplicateCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"CellID"];
    if([self.arr_contact containsObject: [self.arr_contact objectAtIndex:indexPath.row]]){
    
    
    NSDictionary *dict = [self.arr_contact objectAtIndex:indexPath.row];
    
    NSString *name = [NSString stringWithFormat:@"%@",[dict objectForKey:@"name"]];
    //    cell.image.image = [UIImage imageNamed:@"icon_contactCard"];
    cell.lbl_name.text = name;
    NSArray *arr = [cell.lbl_name.text componentsSeparatedByString:@" "];
    cell.lbl_detail.text = @"";
    cell.lbl_fill.backgroundColor = (UIColor *)[arr_color objectAtIndex:(indexPath.row%11)];
    if(arr.count > 1)
    {
        NSString *sec = (NSString *)[arr lastObject];
        NSString *firstObj =  ([[arr firstObject] length] > 0) ? [[arr firstObject] substringToIndex:1] : @"";
        NSString *str = [NSString stringWithFormat:@"%@%@",firstObj, (sec.length > 0) ? [sec substringToIndex:1] : @""];
        cell.lbl_initial.text = str;
    }else{
        cell.lbl_initial.text = [cell.lbl_name.text substringToIndex:1];
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    if([self.pageCameFrom isEqualToString:@"Delete Contact"]){
        [cell.view_selected setHidden:([[dict objectForKey:@"isSelect"] integerValue] == 0)];
//        cell.rightUtilityButtons = [self deleteButtons];
//        cell.delegate = self;
//        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
        }
    return  cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 55;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(![self.pageCameFrom isEqualToString:@"Delete Contact"]){
        NSDictionary *dict = [self.arr_contact objectAtIndex:indexPath.row];
        CNContact *contact = (CNContact *)[dict objectForKey:@"contact"];
        [[ContactAction sharedContacts] viewOrEditContact:contact andIsForEdit:([_pageCameFrom isEqualToString:@"Edit Contact"])];
    }else{
        DuplicateCell *cell = [self.tableview cellForRowAtIndexPath:indexPath];
        NSMutableDictionary *dict = [[_arr_contact objectAtIndex:indexPath.row] mutableCopy];
        [cell.view_selected setHidden:(![[dict objectForKey:@"isSelect"] isEqualToString:@"0"])];
        [dict setObject:([[dict objectForKey:@"isSelect"] isEqualToString:@"0"]) ? @"1" : @"0" forKey:@"isSelect"];
        [_arr_contact replaceObjectAtIndex:indexPath.row withObject:dict];
        NSPredicate *pred = [NSPredicate predicateWithFormat:@"isSelect == %@ || isSelect == 1",@"1"];
        NSArray *arr = [_arr_contact filteredArrayUsingPredicate:pred];
        [self.view_header.lbl_right setText:(arr.count == _arr_contact.count) ? @"None":@"All"];
    }
}

-(void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(![self.pageCameFrom isEqualToString:@"Delete Contact"]){
        NSDictionary *dict = [self.arr_contact objectAtIndex:indexPath.row];
        CNContact *contact = (CNContact *)[dict objectForKey:@"contact"];
        [[ContactAction sharedContacts] viewOrEditContact:contact andIsForEdit:([_pageCameFrom isEqualToString:@"Edit Contact"])];
    }else{
        DuplicateCell *cell = [self.tableview cellForRowAtIndexPath:indexPath];
        NSMutableDictionary *dict = [[_arr_contact objectAtIndex:indexPath.row] mutableCopy];
        [cell.view_selected setHidden:(![[dict objectForKey:@"isSelect"] isEqualToString:@"0"])];
        [dict setObject:([[dict objectForKey:@"isSelect"] isEqualToString:@"0"]) ? @"1" : @"0" forKey:@"isSelect"];
        [_arr_contact replaceObjectAtIndex:indexPath.row withObject:dict];
        NSPredicate *pred = [NSPredicate predicateWithFormat:@"isSelect == %@ || isSelect == 1",@"1"];
        NSArray *arr = [_arr_contact filteredArrayUsingPredicate:pred];
        [self.view_header.lbl_right setText:(arr.count == _arr_contact.count) ? @"None":@"All"];
    }
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(SWTableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.row == 0 && isHint && [self.pageCameFrom isEqualToString:@"Delete Contact"])
    {
        [cell performSelector:@selector(animateCell:) withObject:cell afterDelay:1.0];
        isHint = NO;
    }
}
#pragma mark - swtableviewcelldelegate
-(void)swipeableTableViewCell:(SWTableViewCell *)cell didTriggerRightUtilityButtonWithIndex:(NSInteger)index
{
    NSIndexPath *indexPath = [_tableview indexPathForCell:cell];
    NSDictionary *dict = [self.arr_contact objectAtIndex:indexPath.row];
    UIAlertController * alert = [UIAlertController
                                 alertControllerWithTitle:@"Delete"
                                 message:[NSString stringWithFormat:@"Are You Sure Want to Delete! %@", [dict objectForKey:@"name"]]
                                 preferredStyle:UIAlertControllerStyleAlert];

    UIAlertAction* yesButton = [UIAlertAction
                                actionWithTitle:@"Yes"
                                style:UIAlertActionStyleDefault
                                handler:^(UIAlertAction * action) {
                                    NSIndexPath *indexPath = [self.tableview indexPathForCell:cell];
                                    NSDictionary *dict = [self.arr_contact objectAtIndex:indexPath.row];
                                    CNContact *contact = (CNContact *)[dict objectForKey:@"contact"];
                                    [[ContactAction sharedContacts]deleteSingleContact:contact WithCompletionHandler:^(CNContact *contact, BOOL completed) {
                                        [SVProgressHUD showSuccessWithStatus:@"Deleted Successfully"];
                                    }];
                                    [self.arr_contact removeObjectAtIndex:indexPath.row];
                                    [self.tableview reloadData];                                }];
    
    UIAlertAction* noButton = [UIAlertAction
                               actionWithTitle:@"Cancel"
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction * action) {
                               }];
    
    [alert addAction:yesButton];
    [alert addAction:noButton];
    [self presentViewController:alert animated:YES completion:nil];
}

- (BOOL)swipeableTableViewCellShouldHideUtilityButtonsOnSwipe:(SWTableViewCell *)cell
{
    return YES;
}


#pragma mark - HeaderViewDelegate
-(void)btnLeftClicked
{
    [self slideView];
}
- (IBAction)btn_delete:(id)sender {
    NSPredicate *pred = [NSPredicate predicateWithFormat:@"isSelect == %@ || isSelect == 1",@"1"];
    NSArray *arr = [_arr_contact filteredArrayUsingPredicate:pred];
    if(arr.count == 0 || arr == nil)
    {
        [self.view makeToast:@"Please select any contact"];
    }else{
        NSLog(@"delete");
        [SVProgressHUD showWithStatus:@"deleting"];
        [arr enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            NSDictionary *dict = [[arr objectAtIndex:idx] copy];
            NSLog(@"dic %@",dict);
            CNContact *contact= [[dict objectForKey:@"contact"] copy];
            [[ContactAction sharedContacts]deleteSingleContactFile:contact];
            [_arr_contact removeObject:dict];
        }];
        [SVProgressHUD performSelector:@selector(dismiss) withObject:nil afterDelay:2.0];
        [self.tableview performSelector:@selector(reloadData) withObject:nil afterDelay:2.0];
    }
}

-(void)btnRigtClicked
{
    if([_view_header.lbl_right.text  isEqualToString:@"All"]){
        [self addKeySelected:@"1"];
        [_view_header.lbl_right setText:@"None"];
    }else{
        [self addKeySelected:@"0"];
        [_view_header.lbl_right setText:@"All"];
    }
    [self.tableview reloadData];
}
@end

