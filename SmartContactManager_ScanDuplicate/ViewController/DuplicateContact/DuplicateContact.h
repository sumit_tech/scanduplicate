//
//  DuplicateContact.h
//  SmartContactManager_ScanDuplicate
//
//  Created by 8_Sandhya on 13/11/18.
//  Copyright © 2018 Techathalon. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MasterVC.h"
@protocol DuplicateContactDelegate <NSObject>
@optional
-(void)rescanAfterMerge:(BOOL)ismerge;
@end
@interface DuplicateContact : MasterVC <UIDocumentInteractionControllerDelegate>
@property (nonatomic, assign) NSInteger barItemIndex;
@property (strong, nonatomic) NSMutableArray *arr_duplicate;
@property (retain) UIDocumentInteractionController * documentInteractionController;
@property(weak,nonatomic)id<DuplicateContactDelegate>delegate;

@end
