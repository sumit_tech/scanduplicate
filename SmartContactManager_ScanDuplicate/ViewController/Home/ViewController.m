//
//  ViewController.m
//  SmartContactManager_ScanDuplicate
//
//  Created by 8_Sandhya on 11/09/18.
//  Copyright © 2018 Techathalon. All rights reserved.
//

#import "ViewController.h"
#import "view_duplicateCell.h"
#import "MBCircularProgressBarView.h"
#import "CRGradientLabel.h"
#import "DuplicateContact.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKShareKit/FBSDKShareKit.h>
#import <GoogleMobileAds/GoogleMobileAds.h>
@interface ViewController () <HeaderViewDelegate, DuplicateContactDelegate,GADBannerViewDelegate>//,UITableViewDelegate, UITableViewDataSource>
{
    NSMutableArray *arr_dupName, *arr_dupNum, *arr_dupEmail, *arr_display;
    NSInteger baritemIndex;
    CGFloat nameHeight;
    NSString *baritemName;
    NSInteger count;
}
@property (weak, nonatomic) IBOutlet UIView *adsmobView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *cnst_heightadsmob;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *cnst_mergeBottom;
@property (strong, nonatomic) IBOutlet UIView *view_container;
@property (strong, nonatomic) IBOutlet UIView *view_numberTitle;
@property (strong, nonatomic) IBOutlet HeaderView *view_header;
@property (strong, nonatomic) IBOutlet UIView *view_DupTitlw;
@property (strong, nonatomic) IBOutlet UIView *view_name;
@property (strong, nonatomic) IBOutlet UIView *view_email;
@property (strong, nonatomic) IBOutlet UIView *view_number;
//@property (strong, nonatomic) IBOutlet UITableView *tableview;
@property (strong, nonatomic) IBOutlet UILabel *lbl_nameCount;
@property (strong, nonatomic) IBOutlet UILabel *lbl_numberCount;
@property (strong, nonatomic) IBOutlet UILabel *lbl_emailCount;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *cnst_nameWidth;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *cnst_numWidth;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *cnst_emailWidth;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *cnst_lineX;
@property (strong, nonatomic) IBOutlet UIView *view_parent;
@property (strong, nonatomic) IBOutlet MBCircularProgressBarView *view_progress;
@property (strong, nonatomic) IBOutlet UIButton *btn_emailTitle;
@property (strong, nonatomic) IBOutlet UIButton *btn_numberTitle;
@property (strong, nonatomic) IBOutlet UIButton *btn_nameTitle;
@property (strong, nonatomic) IBOutlet UIButton *btn_merge;
@property (strong, nonatomic) IBOutlet UIView *view_merge;
@property (strong, nonatomic) IBOutlet CRGradientLabel *lbl_merge;
@property (strong, nonatomic) IBOutlet UIView *view_scan;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    arr_dupNum = [[NSMutableArray alloc]init];
    arr_dupName = [[NSMutableArray alloc]init];
    arr_dupEmail = [[NSMutableArray alloc]init];
    // Do any additional setup after loading the view, typically from a nib.
    [self.view_header changeLeftIconImage:[UIImage imageNamed:@"icon_menu"]];
//    [self.view_header changeRightIconImage:[UIImage imageNamed:@"icon_refresh"]];
    [self.view_header setDelegate:self];
    [self.view_header setTitle:@""];
    [self.lbl_nameCount setText:@"0"];
    [self.lbl_numberCount setText:@"0"];
    [self.lbl_emailCount setText:@"0"];
    [[viewModification sharedViews]changeViewtoCornerRadiusWithShadow:10.0 AndBorder:0.75 andBorderColor:[UIColor darkGrayColor] toView:self.view_DupTitlw andShadowRadius:2.0 andShadowOpacity:0.8];
    [[viewModification sharedViews]changeViewtoCornerRadiusWithShadow:10.0 AndBorder:0.75 andBorderColor:[UIColor darkGrayColor] toView:self.view_name andShadowRadius:2.0 andShadowOpacity:0.8];
    [[viewModification sharedViews]changeViewtoCornerRadiusWithShadow:10.0 AndBorder:0.75 andBorderColor:[UIColor darkGrayColor] toView:self.view_email andShadowRadius:2.0 andShadowOpacity:0.8];
    [[viewModification sharedViews]changeViewtoCornerRadiusWithShadow:10.0 AndBorder:0.75 andBorderColor:[UIColor darkGrayColor] toView:self.view_number andShadowRadius:2.0 andShadowOpacity:0.8];
    baritemIndex = 0;
    [[viewModification sharedViews]setCornerRadius:20 andView:self.view_merge];
    [[viewModification sharedViews]setCornerRadius:20 andView:self.view_scan];
    [self nameUIUpdate:1];
  
    [self scan];
//    [self showAlertOfScan:@"Scan Contact" withMessage:@"Do you want to scan your contacts ?"];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    count= [[[PlistHandler sharedObject]getValueFromPlistByKey:@"backup_count"] integerValue];
    [self secondMethod];
    
}
-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:YES];
    [self performSelector:@selector(shadowHeaderView:) withObject:self.view_header afterDelay:0.5];
    [self performSelectorOnMainThread:@selector(hideNavigationBar) withObject:nil waitUntilDone:NO];
    
    [self setBackgroundGradient:self.view_email color1Red:3.0    color1Green:170.0 color1Blue:175.0 color2Red:100.0 color2Green:214.0 color2Blue:178.0 alpha:1.0];
    [self setBackgroundGradient:self.view_parent color1Red:223.0 color1Green:89.0 color1Blue:108.0 color2Red:255.0 color2Green:178.0 color2Blue:57.0 alpha:1.0];
//    [self setBackgroundGradient:self.view_merge color1Red:132.0 color1Green:120.0 color1Blue:108.0 color2Red:220.0 color2Green:215.0 color2Blue:211.0 alpha:1.0];
    [self setBackgroundGradient:self.view_scan color1Red:132.0 color1Green:120.0 color1Blue:108.0 color2Red:220.0 color2Green:215.0 color2Blue:211.0 alpha:1.0 withName:@"common"];

    [self setBackgroundGradient:self.view_number color1Red:48.0    color1Green:145.0 color1Blue:213.0 color2Red:71.0 color2Green:175.0 color2Blue:232.0 alpha:1.0];
    [self setBackgroundGradient:self.view_name color1Red:151.0   color1Green:56.0 color1Blue:181.0 color2Red:212.0 color2Green:76.0 color2Blue:180.0 alpha:1.0];
    self.cnst_mergeBottom.constant = (FRAMEHEIGHT == 568) ? 10.0 : FRAMEHEIGHT *0.05;
    [self.view layoutIfNeeded];
    [self returnbannerView:self->_adsmobView :self];
//    dispatch_async(dispatch_get_main_queue(), ^{
//    });
    
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)hideNavigationBar{
    [app.navController setNavigationBarHidden:YES animated:YES];
}

-(void)setCountOnLabel:(UILabel *)lbl andText:(NSInteger)count
{
    [lbl setHidden:NO];
    [lbl setText:[NSString stringWithFormat:@"%lu",count]];
    CGSize size = [lbl intrinsicContentSize];
    if(size.width > 26){
        switch (lbl.tag) {
            case 1:{
                self.cnst_nameWidth.constant = size.width + 5;
            }break;
            case 2:{
                _cnst_numWidth.constant = size.width + 5;
            }break;
            case 3:{
                self.cnst_emailWidth.constant = size.width + 5;
            }break;
            default:
                break;
        }
    }
    [[viewModification sharedViews] changeViewtoCornerRadius:lbl.frame.size.height/2 AndBorder:1 andBorderColor:[UIColor whiteColor] toView:lbl];
    
    //    [self.view setNeedsUpdateConstraints];
    if(count == 0)
        [lbl setHidden:YES];
    
    
}




-(void)reloadTableview
{
//    [self.tableview reloadData];
//    switch (baritemIndex) {
//        case 1:
//            [self setBackgroundGradient:self.tableview color1Red:151.0   color1Green:56.0 color1Blue:181.0 color2Red:212.0 color2Green:76.0 color2Blue:180.0 alpha:0.5];
//            break;
//        case 2:
//            [self setBackgroundGradient:self.tableview color1Red:48.0    color1Green:145.0 color1Blue:213.0 color2Red:71.0 color2Green:175.0 color2Blue:232.0 alpha:0.5];
//            break;
//        case 3:
//            [self setBackgroundGradient:self.tableview color1Red:3.0    color1Green:170.0 color1Blue:175.0 color2Red:100.0 color2Green:214.0 color2Blue:178.0 alpha:0.5];
//            break;
//
//        default:
//            break;
//    }
}

#pragma mark - HelperMethd

-(void)scan
{
    [SVProgressHUD showWithStatus:@"Scanning ..."];
    [[ContactAction sharedContacts] newScanWithCompletionHandler:^(NSMutableArray *arr_name, NSMutableArray *arr_number, NSMutableArray *arr_email, NSMutableArray *arr_contact, int result) {
        if(result == 2){
            dispatch_async(dispatch_get_main_queue(), ^{
                
                self->arr_dupName = arr_name;
                self->arr_dupNum = arr_number;
                self->arr_dupEmail = arr_email;
                [SVProgressHUD dismiss];
                NSLog(@"%ld, %ld, %ld, %ld",arr_name.count,arr_number.count,arr_email.count,arr_contact.count);
                [self.view_progress setValue:arr_name.count];
                [self.view_progress setMaxValue:arr_contact.count];
                [self.view_progress setUnitString:[NSString stringWithFormat:@"/%lu",(unsigned long)arr_contact.count]];
                [self.lbl_nameCount setText:[NSString stringWithFormat:@"%lu",(unsigned long)arr_name.count]];
                [self.lbl_numberCount setText:[NSString stringWithFormat:@"%lu",(unsigned long)arr_number.count]];
                [self.lbl_emailCount setText:[NSString stringWithFormat:@"%lu",(unsigned long)arr_email.count]];
                //            [self nameUIUpdate:1];
                [self.view layoutSubviews];
                [SVProgressHUD performSelector:@selector(dismiss) withObject:nil afterDelay:4.0];
            });
        }else{
            dispatch_async(dispatch_get_main_queue(), ^{
                [SVProgressHUD performSelector:@selector(dismiss) withObject:nil afterDelay:2.0];
            });
        }
    }];
}

-(NSArray *)removeGradientLayer
{
    NSMutableArray *layers = [self.view_merge.layer.sublayers mutableCopy];
//    [layers enumerateObjectsUsingBlock:^(__kindof CALayer * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
//        if ([obj isKindOfClass:[CAGradientLayer class]]) {
//            [layers removeObject:obj];
//            self.view_merge.layer.sublayers = [layers copy];
//        }
//    }];
for(CALayer *layer in layers)
{
    if ([layer isKindOfClass:[CAGradientLayer class]])
    [layers removeObject:layer];
}
    return [layers copy];
}
-(void)showAlertOfScan:(NSString*)alertTitle withMessage:(NSString*)alertMessage{
    [[AlertControllerDesign sharedObject]showAlertControllerWithTitle:alertTitle withMessage:alertMessage withButtons:[NSArray arrayWithObjects:@"Cancel",@"Scan", nil] completionHandler:^(NSString *string) {
        if ([string isEqualToString:@"Scan"]){
            [self scan];
        }
    }];
}
#pragma mark admobDelegate
- (void)adViewDidReceiveAd:(GADBannerView *)adView {
    adView.alpha = 0.0;
    [UIView animateWithDuration:1.0 animations:^{
        adView.alpha = 1.0;
        [self.cnst_heightadsmob setConstant:50];
    }];
}
-(void)adView:(GADBannerView *)bannerView didFailToReceiveAdWithError:(GADRequestError *)error{
    NSLog(@"Error %@",error);
}
//#pragma mark shareDialogviewDelegate
//-(void)shareSuccess
//{
//    if([SVProgressHUD isVisible])
//        [SVProgressHUD dismiss];
//
//    [[AlertControllerDesign sharedObject] alertController:@"Backup" withMessage:@"We strongly recommend you to take backup of contact merge. Would like to take backup ?" andButtonOne:@"Ok" andButtontwo:@"Cancel" WithCompletionHandler:^(BOOL completed) {
//        if(completed)
//        {
//            if(self->count < 3){
//                [[ContactList sharedContacts] fetchAllContacts:YES WithCompletionHandler:^(int result, NSMutableArray *arr) {
//                    if(result == 2 ){
//                        [[ContactList sharedContacts] generateAndSaveVCF:arr WithUSerID:[[PlistHandler sharedObject]getValueFromPlistByKey:@"user_id"] andCompletionHandler:^(NSString *filename, BOOL completed) {
//                            dispatch_async(dispatch_get_main_queue(), ^{
//                                if([SVProgressHUD isVisible])
//                                    [SVProgressHUD dismiss];
//
//                                [[ContactList sharedContacts]uploadFile:[[PlistHandler sharedObject]getValueFromPlistByKey:@"registered_email"] WithCompletionHandler:^(int result, NSMutableDictionary *response) {
//                                    switch (result) {
//                                        case 1:
//                                            [SVProgressHUD showErrorWithStatus:@"Some error occured. Please try again"];
//                                            break;
//                                        case 2:
//                                            [SVProgressHUD showErrorWithStatus:@"Internet issue. Please try again"];
//                                            break;
//                                        case 3:
//                                            [SVProgressHUD showErrorWithStatus:@"Some error occured. Please try again"];
//                                            break;
//                                        case 4:{
//                                            [SVProgressHUD dismiss];
//                                            [SVProgressHUD showSuccessWithStatus:@"VCF file uploaded."];                                           dispatch_async(dispatch_get_main_queue(),^{
//                                                [[PlistHandler sharedObject]setBackupCount:++self->count];
//                                            });
//                                        }
//                                            break;
//                                        default:
//                                            break;
//                                    }
//                                }];
//                                //                            if([[[PlistHandler sharedObject]getValueFromPlistByKey:@"isLogin"] boolValue]){
//                                //                                if(completed)
//                                //                                    NSLog(@"file fetch completed");
//                                //                                else
//                                //                                    NSLog(@"file fetch error");
//                                //                                [self showShareView];
//                                //                                self.shareview.delegate = self;
//                                //                                //                        [[ContactList sharedContacts]uploadFile:[[PlistHandler sharedObject]getValueFromPlistByKey:@"registered_email"]];
//                                //                            }else{
//                                //                                [[AlertControllerDesign sharedObject] alertControllerForLoginWithCompletionHandler:^(NSMutableDictionary *dict, BOOL completed) {
//                                //                                    if(completed){
//                                //                                        self->httpHelper = [[HttpHelper alloc]initWithMUrl:registerUserURL Dictionary:[dict copy] isloading:YES api_number:registerUserAPI message:@"Loading backup"];
//                                //                                        self->httpHelper.delegate = self;
//                                //                                    }
//                                //                                }];
//                                //                            }
//                            });
//                        }];
//                    }
//                }];
//            }else{
//                dispatch_async(dispatch_get_main_queue(), ^{
//                    [[AlertControllerDesign sharedObject] alertController:@"User can save only 3 back up file. please clear any backup to take more backup." withMessage:@"" andButtonOne:@"Ok" andButtontwo:@"Cancel" WithCompletionHandler:^(BOOL completed) {
//                        if(completed)
//                            [self redirectToBackupHistory];
//                        else
//                            NSLog(@"clik 1");
//                    }];
//                });
//            }
//        }
//        [self mergeAllContact];
//    }];
//}
//
//-(void)shareFailed
//{
//    NSLog(@"share failed");
//}

//-(void)mergeAllContact
//{
//    switch (baritemIndex) {
//        case 1:
//        {
//            for(NSDictionary *dict in arr_dupName){
//                [[ContactAction sharedContacts] mergeDuplicateContactByName:dict WithCompletionHandler:^(CNContact *contact, BOOL completed) {
//                }];
//            }
//        }
//            break;
//        case 2:
//        {
//            for(NSDictionary *dict in arr_dupNum){
//                [[ContactAction sharedContacts] mergeDuplicateContactByNumber:dict WithCompletionHandler:^(BOOL completed) {}];
//            }
//        }
//            break;
//        case 3:
//        {
//            for(NSDictionary *dict in arr_dupEmail){
//                [[ContactAction sharedContacts] mergeDuplicateContactByEmail:dict WithCompletionHandler:^(BOOL completed) {}];
//            }
//        }
//            break;
//        default:
//            [self.view makeToast:@"please select any option"];
//            break;
//    }
//    [self scan];
//}

#pragma mark -IBaction
//- (IBAction)btn_mergeAll:(id)sender {
//    NSString *msg = [NSString stringWithFormat:@"Are you sure want to merge all contact by %@",(baritemIndex == 1) ? @"name" : (baritemIndex == 2) ? @"number" : @"email"];
//    [[AlertControllerDesign sharedObject]alertController:@"Info" withMessage:msg andButtonOne:@"Yes" andButtontwo:@"No" WithCompletionHandler:^(BOOL completed) {
//        if(completed)
//        {
//            [self showShareView];
//        }
//    }];
//}
- (IBAction)btn_rateUs:(id)sender {
    NSString *reviewURL = @"https://itunes.apple.com/us/app/id1444870653";
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:reviewURL] options:@{} completionHandler:^(BOOL success) {
        
    }];
}

- (IBAction)btn_scanAll:(id)sender
{
    [self scan];
}

-(void)nameUIUpdate:(int)index
{
    switch (index) {
        case 1:
            {
                baritemIndex = 1;
                baritemName = @"Name";
                
                self.cnst_lineX.constant = 0;
                [UIView animateWithDuration:0.5 animations:^{
                    [self.view layoutIfNeeded];
                    [self.view_progress setValue:self->arr_dupName.count];
                    [self.view_progress setProgressColor:[UIColor colorWithRed:212.0/255.0 green:76.0/255.0 blue:180.0/255.0 alpha:1.0]];
                    [self.btn_nameTitle setBackgroundColor:[UIColor colorWithRed:212.0/255.0 green:76.0/255.0 blue:180.0/255.0 alpha:1.0]];
                    [self.view_merge setBackgroundColor:[UIColor colorWithRed:212.0/255.0 green:76.0/255.0 blue:180.0/255.0 alpha:1.0]];
                    [self.btn_numberTitle setBackgroundColor:[UIColor clearColor]];
                    [self.btn_emailTitle setBackgroundColor:[UIColor clearColor]];
                }];
            }
            break;
        case 2:
        {
            baritemIndex = 2;
            baritemName = @"Number";
            self.cnst_lineX.constant = self.view_numberTitle.frame.origin.x;
            
            [UIView animateWithDuration:0.5 animations:^{
                [self.view layoutIfNeeded];
                [self.view_progress setValue:self->arr_dupNum.count];
                [self.view_progress setProgressColor:[UIColor colorWithRed:71.0/255.0 green:175.0/255.0 blue:232.0/255.0 alpha:1.0]];
                [self.view_merge setBackgroundColor:[UIColor colorWithRed:71.0/255.0 green:175.0/255.0 blue:232.0/255.0 alpha:1.0]];
                [self.btn_numberTitle setBackgroundColor:[UIColor colorWithRed:71.0/255.0 green:175.0/255.0 blue:232.0/255.0 alpha:1.0]];
                [self.btn_nameTitle setBackgroundColor:[UIColor clearColor]];
                [self.btn_emailTitle setBackgroundColor:[UIColor clearColor]];
            }];
        }
            break;
            case 3:
        {
            baritemIndex = 3;
            baritemName = @"Email";
            self.cnst_lineX.constant = self.view_numberTitle.frame.origin.x * 2;
            
            [UIView animateWithDuration:0.5 animations:^{
                [self.view layoutIfNeeded];
                [self.view_progress setValue:self->arr_dupEmail.count];
                [self.view_progress setProgressColor:[UIColor colorWithRed:100.0/255.0 green:214.0/255.0 blue:178.0/255.0 alpha:1.0]];
                [self.view_merge setBackgroundColor:[UIColor colorWithRed:100.0/255.0 green:214.0/255.0 blue:178.0/255.0 alpha:1.0]];
                [self.btn_emailTitle setBackgroundColor:[UIColor colorWithRed:100.0/255.0 green:214.0/255.0 blue:178.0/255.0 alpha:1.0]];
                [self.btn_numberTitle setBackgroundColor:[UIColor clearColor]];
                [self.btn_nameTitle setBackgroundColor:[UIColor clearColor]];
            }];
        }
            break;
        default:
            break;
    }
    [self.view layoutSubviews];
    [self.view setNeedsLayout];
    [self.view setNeedsDisplay];
}

- (IBAction)btn_EmailTitle:(id)sender {
    [self nameUIUpdate:3];
}

- (IBAction)btn_NameTitle:(id)sender {
    [self nameUIUpdate:1];
}

- (IBAction)btn_NumberTitle:(id)sender {
    [self nameUIUpdate:2];
}

- (IBAction)btn_NameCOunt:(id)sender {
    [self nameUIUpdate:1];
    [self btn_mergeContact:sender];
}
- (IBAction)btn_NumberCOunt:(id)sender {
    [self nameUIUpdate:2];
    [self btn_mergeContact:sender];
}
- (IBAction)btn_EmailCOunt:(id)sender {
    [self nameUIUpdate:3];
    [self btn_mergeContact:sender];
}

- (IBAction)btn_mergeContact:(id)sender {
    DuplicateContact *dupContact = [[DuplicateContact alloc]init];
    switch (baritemIndex) {
        case 1:
            dupContact.arr_duplicate = arr_dupName;
            break;
        case 2:
            dupContact.arr_duplicate = arr_dupNum;

            break;
        case 3:
            dupContact.arr_duplicate = arr_dupEmail;
            break;
        default:
            break;
    }
    dupContact.barItemIndex = baritemIndex;
    dupContact.delegate = self;
    [app.navController pushViewController:dupContact animated:YES];
}

- (IBAction)btn_fbShare:(id)sender {
    FBSDKShareLinkContent *content = [[FBSDKShareLinkContent alloc] init];
    content.contentURL = [NSURL URLWithString:@"https://itunes.apple.com/us/app/id1443756011"];
    [FBSDKShareDialog showFromViewController:self withContent:content delegate:nil];
}
- (IBAction)btn_whatsApp:(id)sender {
    NSString *msg= @"Good News.!!!. My Contact Scan Duplicate. It FREE for few days.Download it. https://itunes.apple.com/us/app/id1443756011";
    NSString* encodedUrl = [msg stringByAddingPercentEscapesUsingEncoding:
                            NSUTF8StringEncoding];
    NSURL *whatsappURL = [NSURL URLWithString:[NSString stringWithFormat:@"whatsapp://send?text=%@",encodedUrl]];
    if ([[UIApplication sharedApplication] canOpenURL: whatsappURL]) {
        [[UIApplication sharedApplication] openURL: whatsappURL options:@{} completionHandler:nil];
    }
    else
    {
        UIAlertController *whatsapp_alert = [UIAlertController alertControllerWithTitle:@"Info" message:@"Whatsapp not installed" preferredStyle:UIAlertControllerStyleAlert] ;
        [whatsapp_alert addAction:[UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            [self->app.navController dismissViewControllerAnimated:YES completion:nil];
        } ]];
        [app.navController presentViewController:whatsapp_alert animated:YES completion:nil];
    }
}
- (IBAction)btn_more:(id)sender {
    [[AlertControllerDesign sharedObject] presentActivityController];
}

#pragma mark -HEaderViewDelegate

-(void)btnRigtClicked
{
    [self scan];
}
-(void)btnLeftClicked
{
    [self slideView];
}

#pragma mark - DuplicateContactDelegate
-(void)rescanAfterMerge:(BOOL)ismerge
{
    if (ismerge) {
        [self scan];
    }
}
-(void)rescanAfterMerge
{
    [self scan];
}

@end
