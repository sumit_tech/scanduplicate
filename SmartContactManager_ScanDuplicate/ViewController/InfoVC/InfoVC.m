//
//  InfoVC.m
//  SmartContactManager_Backup
//
//  Created by 8_Sandhya on 06/09/18.
//  Copyright © 2018 Techathalon. All rights reserved.
//

#import "InfoVC.h"
#import "HeaderView.h"
#import "ContactCell.h"
#import "PrivacyPolicyFaq.h"
#import <MessageUI/MessageUI.h>
#import <MessageUI/MFMailComposeViewController.h>
@interface InfoVC ()<HeaderViewDelegate, UITableViewDelegate, UITableViewDataSource, MFMailComposeViewControllerDelegate>
{
    NSArray *arr_icon, *arr_title;
}
@property (strong, nonatomic) IBOutlet HeaderView *view_header;
@property (strong, nonatomic) IBOutlet UITableView *tableview;

@end

@implementation InfoVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self performSelector:@selector(shadowHeaderView:) withObject:self.view_header afterDelay:0.5];
    [self.view_header changeLeftIconImage:[UIImage imageNamed:@"icon_menu"]];
    [self.view_header setTitle:@"Info"];
    self.view_header.delegate = self;
    [self.tableview registerNib:[UINib nibWithNibName:@"ContactCell" bundle:nil] forCellReuseIdentifier:CONTACTCELL_ID];

    arr_icon = [[NSArray alloc]initWithObjects:@"icon_feedBack", @"icon_rate",@"icon_friend", nil];
    arr_title = [[NSArray alloc]initWithObjects:@"Feedback", @"Rate us", @"Tell a Friend", nil];
    self.tableview.delegate = self;
    self.tableview.dataSource = self;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - UITableViewDelegate
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    switch (section) {
        case 0:
            return arr_icon.count;
            break;
        case 1:
            return 2;
            break;
        default:
            return 0;
            break;
    }
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    ContactCell *cell = (ContactCell *)[tableView dequeueReusableCellWithIdentifier:CONTACTCELL_ID];
    if(!cell)
        cell = [[ContactCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CONTACTCELL_ID];
    
    switch (indexPath.section) {
        case 0:
        {
            cell.image.image = [UIImage imageNamed:[arr_icon objectAtIndex:indexPath.row]];
            cell.lbl_name.text = [arr_title objectAtIndex:indexPath.row];
        }
            break;
        case 1:
        {
            cell.image.image = [UIImage imageNamed:(indexPath.row == 0) ? @"icon_privacy" : @"icon_faq"];
            cell.lbl_name.text = (indexPath.row == 0) ? @"Pricacy Policy" : @"Faq";
        }
            break;
        default:
            break;
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 55;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *view = [[UIView alloc]initWithFrame:CGRectMake(0, 0, FRAMEWIDTH, 45)];
    view.backgroundColor = [UIColor colorWithRed:230.0/255.0 green:230.0/255.0 blue:230.0/255.0 alpha:0.7];
    UILabel *label = [[UILabel alloc]initWithFrame: CGRectMake(16, 0, FRAMEWIDTH - 32, 45)];
    label.text = (section == 0) ? @"Info" : @"App Details";
    label.textColor = [UIColor darkGrayColor];
    [view addSubview:label];
    return view;
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 45;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *url=@"https://itunes.apple.com/us/app/id1032947867";

    switch (indexPath.section) {
        case 0:
        {
            switch (indexPath.row) {
                case 0:
                    {
                        if([MFMailComposeViewController canSendMail]) {
                            MFMailComposeViewController *mailCont = [[MFMailComposeViewController alloc] init];
                            mailCont.mailComposeDelegate = self;
                            
                            [mailCont setSubject:[NSString stringWithFormat:@"FeedBack - from %@",[[PlistHandler sharedObject]getValueFromPlistByKey:@"registered_email"]]];
                            [mailCont setToRecipients:[NSArray arrayWithObject:@"info@techathalon.com"]];
                            [mailCont setMessageBody:[NSString stringWithFormat:@"The most secure contact backup. Download Smart Contact Manager - Backup app %@", url] isHTML:NO];
                            
                            [app.navController presentViewController:mailCont animated:YES completion:nil];
                        }
                    }
                    break;
                case 1:{
                    NSString *reviewURL = @"https://itunes.apple.com/us/app/id1032947867";
                    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:reviewURL] options:@{} completionHandler:^(BOOL success) {
                        
                    }];
                }
                    break;
                case 2:
                {
                    [[AlertControllerDesign sharedObject] presentActivityController];
                }
                    break;
                default:
                    break;
            }
        }
            break;
        case 1:
        {
            PrivacyPolicyFaq *privacy = [[PrivacyPolicyFaq alloc]init];
            privacy.urlString = (indexPath.row == 0) ? privacyPath : faqPath;
            privacy.title = (indexPath.row == 0) ? @"Pricacy Policy" : @"Faq";
            [app.navController pushViewController:privacy animated:YES];
        }
            break;
        default:
            break;
    }
}

-(void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self tableView:tableView didSelectRowAtIndexPath:indexPath];
}
#pragma mark - HeaderViewDelegate
-(void)btnLeftClicked
{
    [self slideView];
}

- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error {
    [controller dismissViewControllerAnimated:YES completion:nil];
}
@end
