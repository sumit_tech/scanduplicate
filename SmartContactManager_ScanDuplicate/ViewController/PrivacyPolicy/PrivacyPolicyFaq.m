//
//  PrivacyPolicyFaq.m
//  SmartContactManager_Backup
//
//  Created by 8_Sandhya on 06/09/18.
//  Copyright © 2018 Techathalon. All rights reserved.
//

#import "PrivacyPolicyFaq.h"
#import "HeaderView.h"
#import <WebKit/WebKit.h>
@interface PrivacyPolicyFaq ()<HeaderViewDelegate>
@property (strong, nonatomic) IBOutlet HeaderView *view_header;
@property (strong, nonatomic) IBOutlet WKWebView *webview;

@end

@implementation PrivacyPolicyFaq

- (void)viewDidLoad {
    [super viewDidLoad];
    [self performSelector:@selector(shadowHeaderView:) withObject:self.view_header afterDelay:0.5];
    [self.view_header changeLeftIconImage:[UIImage imageNamed:@"logo_back"]];
    [self.view_header setTitle:self.title];
    self.view_header.delegate = self;
    NSURL *nsurl=[NSURL URLWithString:_urlString];
    NSURLRequest *nsrequest=[NSURLRequest requestWithURL:nsurl];
    [self.webview loadRequest:nsrequest];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark -HeaderViewDelegate
-(void)btnLeftClicked{
    [app.navController popViewControllerAnimated:YES];
}

@end
