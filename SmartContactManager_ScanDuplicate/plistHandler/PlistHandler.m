//
//  PlistHandler.m
//  SmartContactManager_Backup
//
//  Created by 8_Sandhya on 07/09/18.
//  Copyright © 2018 Techathalon. All rights reserved.
//

#import "PlistHandler.h"
@interface PlistHandler ()

@end
@implementation PlistHandler
+ (id)sharedObject {
    static PlistHandler *sharedMyViews = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{ //for first time create shared instance object
        sharedMyViews = [[self alloc] init];
    });
    return sharedMyViews;
}

-(void) savePlistDataWithData:(NSDictionary *)userdata
{
    NSArray *paths=NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,NSUserDomainMask , YES);
    NSString *documentDirectory=[paths objectAtIndex:0];
    NSString *pListPath=[documentDirectory stringByAppendingPathComponent:@"SCMBackup.plist"];
    NSFileManager *fileManager=[NSFileManager defaultManager];
    if ([fileManager fileExistsAtPath:pListPath]) {
        self.pList=[NSMutableDictionary dictionaryWithContentsOfFile:pListPath];
        if([self.pList count] == 0){
            self.pList=[[NSMutableDictionary alloc]initWithDictionary:userdata];
        } else {
            [_pList addEntriesFromDictionary:userdata];
        }
//        [self.pList writeToFile:pListPath atomically:YES];
    }else{
        
        if([self.pList count] == 0){
            self.pList=[[NSMutableDictionary alloc]initWithDictionary:userdata];
        } else {
            [self.pList addEntriesFromDictionary:userdata];
        }
//        [self.pList writeToFile:pListPath atomically:YES];
    }
    [self.pList setObject:@"1" forKey:@"isLogin"];
    [self.pList setObject:@"1" forKey:@"reminderSelect"];
    [self.pList writeToFile:pListPath atomically:YES];
}

-(NSMutableDictionary *)getPlistData{
    @try {
        NSArray *path=NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentDirectory=[path objectAtIndex:0];
        NSString *plistPath=[documentDirectory stringByAppendingPathComponent:@"SCMBackup.plist"];
        NSFileManager *manager=[NSFileManager defaultManager];
        if([manager fileExistsAtPath:plistPath]){
            _pList = [NSMutableDictionary dictionaryWithContentsOfFile:plistPath];
        }
    } @catch (NSException *exception) {
    }
    return self.pList;
}

-(void)removeDataFromPlist
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains (NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsPath = [paths objectAtIndex:0];
    NSString *plistPath = [documentsPath stringByAppendingPathComponent:@"SCMBackup.plist"];
    NSMutableDictionary *dictionary = [NSMutableDictionary dictionaryWithContentsOfFile:(NSString *)plistPath];
    
    [dictionary removeAllObjects];
    [dictionary setObject:@"0" forKey:@"isLogin"];
    [dictionary setObject:@"0" forKey:@"user_id"];
    [dictionary setObject:@"0" forKey:@"reminderSelect"];
    [dictionary writeToFile:plistPath atomically:YES];
}

-(BOOL)isUserLogin{
    @try {
        NSArray *path=NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentDirectory=[path objectAtIndex:0];
        NSString *plistPath=[documentDirectory stringByAppendingPathComponent:@"SCMBackup.plist"];
        NSFileManager *manager=[NSFileManager defaultManager];
        if([manager fileExistsAtPath:plistPath]){
            self.pList = [NSMutableDictionary dictionaryWithContentsOfFile:plistPath];
        }
    } @catch (NSException *exception) {
        //        NSLog(@"^exception....%@",exception);
    }
    return [[self.pList objectForKey:@"isLogin"] boolValue];
}

//-(NSInteger)getBackupCount{
//    @try {
//        NSArray *path=NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
//        NSString *documentDirectory=[path objectAtIndex:0];
//        NSString *plistPath=[documentDirectory stringByAppendingPathComponent:@"SCMBackup.plist"];
//        NSFileManager *manager=[NSFileManager defaultManager];
//        if([manager fileExistsAtPath:plistPath]){
//            self.pList = [NSMutableDictionary dictionaryWithContentsOfFile:plistPath];
//        }
//    } @catch (NSException *exception) {
//        //        NSLog(@"^exception....%@",exception);
//    }
//    return [[self.pList objectForKey:@"backup_count"] integerValue];
//}
-(void)setBackupCount:(NSInteger)count{
        NSArray *path=NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentDirectory=[path objectAtIndex:0];
        NSString *plistPath=[documentDirectory stringByAppendingPathComponent:@"SCMBackup.plist"];
        NSMutableDictionary *dictionary = [NSMutableDictionary dictionaryWithContentsOfFile:(NSString *)plistPath];
        
        [dictionary setObject:[NSString stringWithFormat:@"%lu",count] forKey:@"backup_count"];
        [dictionary writeToFile:plistPath atomically:YES];
}

//-(NSString *)getEmailID{
//    @try {
//        NSArray *path=NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
//        NSString *documentDirectory=[path objectAtIndex:0];
//        NSString *plistPath=[documentDirectory stringByAppendingPathComponent:@"SCMBackup.plist"];
//        NSFileManager *manager=[NSFileManager defaultManager];
//        if([manager fileExistsAtPath:plistPath]){
//            self.pList = [NSMutableDictionary dictionaryWithContentsOfFile:plistPath];
//        }
//    } @catch (NSException *exception) {
//        //        NSLog(@"^exception....%@",exception);
//    }
//    return [NSString stringWithFormat:@"%@",[self.pList objectForKey:@"registered_email"]];
//}
//
-(NSString *)getUserID{
    @try {
        NSArray *path=NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentDirectory=[path objectAtIndex:0];
        NSString *plistPath=[documentDirectory stringByAppendingPathComponent:@"SCMBackup.plist"];
        NSFileManager *manager=[NSFileManager defaultManager];
        if([manager fileExistsAtPath:plistPath]){
            self.pList = [NSMutableDictionary dictionaryWithContentsOfFile:plistPath];
        }
    } @catch (NSException *exception) {
        //        NSLog(@"^exception....%@",exception);
    }
    return [NSString stringWithFormat:@"%@",[self.pList objectForKey:@"user_id"]];
}

-(NSString *)getValueFromPlistByKey:(NSString *)key{
    @try {
        NSArray *path=NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentDirectory=[path objectAtIndex:0];
        NSString *plistPath=[documentDirectory stringByAppendingPathComponent:@"SCMBackup.plist"];
        NSFileManager *manager=[NSFileManager defaultManager];
        if([manager fileExistsAtPath:plistPath]){
            _pList = [NSMutableDictionary dictionaryWithContentsOfFile:plistPath];
        }
    } @catch (NSException *exception) {
        //        NSLog(@"^exception....%@",exception);
    }
    return [NSString stringWithFormat:@"%@",[self.pList objectForKey:key]];
}

-(void)setValueFromPlistByvalue:(NSString *)value andKey:(NSString *)key{
    NSArray *path=NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentDirectory=[path objectAtIndex:0];
    NSString *plistPath=[documentDirectory stringByAppendingPathComponent:@"SCMBackup.plist"];
    NSMutableDictionary *dictionary = [NSMutableDictionary dictionaryWithContentsOfFile:(NSString *)plistPath];
    
    [dictionary setObject:value forKey:key];
    [dictionary writeToFile:plistPath atomically:YES];
}
@end
